<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Survey;

class SurveySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Survey::insert([
            [
                'title' => 'Опитування щодо задоволення дистанційним навчальним процесом під час карантину',
                'from' => '2020.10.01',
                'to' => '2020.11.30',
                'status'=> 2,
                'created_user'=>'44dde7f4-83c3-e511-867d-001a4be6d04a',
                'updated_user'=>'44dde7f4-83c3-e511-867d-001a4be6d04a'
            ],
            [
                'title' => 'Опитування щодо проблем першокурсників',
                'from' => '2020.10.01',
                'to' => '2020.11.21',
                'status'=> 1,
                'created_user'=>'8248eef4-83c3-e511-867d-001a4be6d04a',
                'updated_user'=>'8248eef4-83c3-e511-867d-001a4be6d04a'
            ]
        ]);
    }
}
