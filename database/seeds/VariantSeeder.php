<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Variant;

class VariantSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $variants = [
            ['відповідає дійсності',
             'переважно відповідає дійсності',
             'частково відповідає дійсності',
             'переважно не відповідає дійсності',
             'категорично не відповідає дійсності'],

            ['Так', 'Ні', 'Напевно'],

            ['Viber', 'Telegram', 'Skype', 'Google Meet', 'Google Hangouts', 'Zoom']
        ];
        $questions = [[1, 6], [3, 4, 8, 9], [2, 5, 7, 10]];

        foreach ($questions as $index_set => $set_questions) {
            foreach($set_questions as $id_ques) {
                foreach ($variants[$index_set] as $variant) {
                    Variant::create([
                        'text' => $variant,
                        'id_ques' => $id_ques
                    ]);
                }
            }
        }
    }
}
