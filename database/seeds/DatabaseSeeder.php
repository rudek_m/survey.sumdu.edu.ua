<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            SurveySeeder::class,
            QuestionSeeder::class,
            VariantSeeder::class,
            UserSeeder::class,
            AnswerSeeder::class
        ]);
    }
}
