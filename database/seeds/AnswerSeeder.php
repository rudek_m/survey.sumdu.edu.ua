<?php

use Illuminate\Database\Seeder;
use App\Models\Survey;
use App\Models\Answer;
use App\Models\User;

class AnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $surveys = Survey::all();
        $users = User::skip(2)->take(2)->get();

        foreach ($surveys as $survey) {
            $questions = $survey->questions;
            foreach ($questions as $question) {
                $variants = $question->variants;
                foreach($users as $user) {
                    if ($question->type === 'multi') {
                        $indexes = range(0, rand(0, count($variants) - 1));
                        shuffle($indexes);
                        foreach ($indexes as $index) {
                            Answer::create([
                                'guid_user' => $user->guid,
                                'id_var' => $variants[$index]->id,
                                'id_ques' => $question->id,
                                'id_survey' => $survey->id,
                            ]);
                        }
                    } else {
                        Answer::create([
                            'guid_user' => $user->guid,
                            'id_var' => $variants[rand(0, count($variants) - 1)]->id,
                            'id_ques' => $question->id,
                            'id_survey' => $survey->id,
                        ]);
                    }
                }
            }
        }
    }
}
