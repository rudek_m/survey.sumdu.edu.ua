<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                'guid' => '44dde7f4-83c3-e511-867d-001a4be6d04a',
                'role' => 'admin'
            ],
            [
                'guid' => '8248eef4-83c3-e511-867d-001a4be6d04a',
                'role' => 'admin'
            ],
            [
                'guid' => 'ac31345f-c580-e711-8194-001a4be6d04a',
                'role' => 'user'
            ],
            [
                'guid' => '9263185e-c580-e711-8194-001a4be6d04a',
                'role' => 'user'
            ]
        ]);
    }
}
