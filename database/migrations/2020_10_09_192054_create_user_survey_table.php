<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_survey', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_survey')->constrained('surveys')->onDelete('cascade');
            $table->index('id_survey');

            $table->string('guid_user', 40)->index('guid_user_idx');


            $table->integer('id_div')->nullable();
            $table->string('name_div', 500)->nullable();
            $table->string('code_div', 50)->nullable();

            $table->integer('id_specialty')->nullable();
            $table->string('name_specialty', 300)->nullable();
            $table->string('code_specialty', 50)->nullable();

            $table->integer('id_group')->nullable();
            $table->string('name_group', 20)->nullable();

            $table->integer('id_degree')->nullable();
            $table->string('name_degree', 50)->nullable();

            $table->integer('id_course')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_survey');
    }
}
