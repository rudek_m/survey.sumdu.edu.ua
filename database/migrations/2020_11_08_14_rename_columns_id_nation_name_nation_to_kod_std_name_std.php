<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnsIdNationNameNationToKodStdNameStd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_survey', function (Blueprint $table) {
            $table->renameColumn('id_nation', 'id_kod_std');
            $table->renameColumn('name_nation', 'name_kod_std');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_survey', function (Blueprint $table) {
            $table->renameColumn('id_kod_std', 'id_nation');
            $table->renameColumn('name_kod_std', 'name_nation');
        });
    }
}
