<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->id();
            $table->string('guid_user', 40)->index('guid_user_idx');
            $table->bigInteger('id_var')->index();
            $table->bigInteger('id_ques')->index();
            $table->foreignId('id_survey')->constrained('surveys')->onDelete('cascade');
            $table->timestamp('created_at')->useCurrent();
            $table->index('id_survey');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
    }
}
