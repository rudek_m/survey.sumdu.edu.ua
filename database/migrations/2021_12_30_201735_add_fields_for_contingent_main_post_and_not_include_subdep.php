<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsForContingentMainPostAndNotIncludeSubdep extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contingents', function (Blueprint $table) {
            $table->boolean('main_post')->default(false)->after('id_categ2');
            $table->boolean('not_include_subdep')->default(false)->after('main_post');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contingents', function (Blueprint $table) {
            $table->dropColumn([
                'main_post',
                'not_include_subdep'
            ]);
        });
    }
}
