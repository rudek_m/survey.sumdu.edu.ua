<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEducationProgramToUserSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_survey', function (Blueprint $table) {
            $table->integer('id_edu_prog')->nullable()->after('code_specialty');
            $table->string('name_edu_prog', 300)->nullable()->after('id_edu_prog');
            $table->string('code_edu_prog', 50)->nullable()->after('name_edu_prog');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_survey', function (Blueprint $table) {
            $table->dropColumn([
                'id_edu_prog',
                'name_edu_prog',
                'code_edu_prog'
            ]);
        });
    }
}
