<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContingentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contingents', function (Blueprint $table) {
            $table->id();
            $table->integer('id_categ1')->default(0);
            $table->integer('id_specialty')->nullable();
            $table->integer('id_group')->nullable();
            $table->integer('id_course')->nullable();
            $table->integer('id_degree')->nullable();
            $table->integer('id_categ2')->default(0);
            $table->integer('id_div')->nullable();
            $table->foreignId('id_survey')->constrained('surveys')->onDelete('cascade');
            $table->index('id_survey');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contingents');
    }
}
