<?php

use Illuminate\Database\Migrations\Migration;

class AddTextTypeQuestion extends Migration
{
    private string $schema_table = 'sv_questions';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE {$this->schema_table} MODIFY COLUMN type ENUM('single', 'multi', 'text') NOT NULL DEFAULT 'single'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE {$this->schema_table} MODIFY COLUMN type ENUM('single', 'multi') NOT NULL DEFAULT 'single'");
    }
}
