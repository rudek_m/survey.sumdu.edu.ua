<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFormLearningAndNation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_survey', function (Blueprint $table) {
            $table->integer('id_nation')->nullable()->after('guid_user');
            $table->string('name_nation', 20)->nullable()->after('id_nation');
            $table->integer('id_form')->nullable()->after('name_degree');
            $table->string('name_form', 20)->nullable()->after('id_form');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_survey', function (Blueprint $table) {
            $table->dropColumn([
                'id_nation',
                'name_nation',
                'id_form',
                'name_form'
            ]);
        });
    }
}
