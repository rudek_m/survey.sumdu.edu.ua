<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFormAndKodStdInContingents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contingents', function (Blueprint $table) {
            $table->integer('id_form')->nullable()->after('name_degree');
            $table->string('name_form', 20)->nullable()->after('id_form');
            $table->integer('id_kod_std')->nullable()->after('name_form');
            $table->string('name_kod_std', 20)->nullable()->after('id_kod_std');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contingents', function (Blueprint $table) {
            $table->dropColumn([
                'id_kod_std',
                'name_kod_std',
                'id_form',
                'name_form'
            ]);
        });
    }
}
