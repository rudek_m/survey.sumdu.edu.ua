<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNameFieldsToContingentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contingents', function (Blueprint $table) {
            $table->string('name_specialty', 300)->nullable()->after('id_specialty');
            $table->string('name_group', 20)->nullable()->after('id_group');
            $table->string('name_degree', 50)->nullable()->after('id_degree');
            $table->string('name_div', 500)->nullable()->after('id_div');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contingents', function (Blueprint $table) {
            $table->dropColumn([
                'name_specialty',
                'name_group',
                'name_degree',
                'name_div'
            ]);
        });
    }
}
