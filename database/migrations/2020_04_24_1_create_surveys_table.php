<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->id();
            $table->string('title', 400);
            $table->date('from')->index();
            $table->date('to')->index();
            $table->tinyInteger('status');
            $table->timestamp('created_at')->useCurrent();
            $table->string('created_user',40);
            $table->timestamp('updated_at')->useCurrent();
            $table->string('updated_user',40);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
