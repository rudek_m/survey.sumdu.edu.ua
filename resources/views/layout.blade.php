<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="google" content="notranslate">
    <meta name="theme-color" content="#444e8b">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--Meta-->
    <meta http-equiv="Cache-Control" content="no-cache"/>
    <meta name="description" content="{{ trans('labels.head.description_shot') }}"/>
    <meta name="keywords" content="{{ trans('labels.head.keywords') }}"/>
    <title>{{ trans('labels.head.title') }}</title>

    <!-- Styles -->
    <link href="{{ config('cabinet.url').'/public/css/jquery-ui.min.css'}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ config('cabinet.url').'/public/css/jquery-ui-cabinet.min.css'}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ config('cabinet.url').'/public/css/cabinet.min.css'}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ mix('css/app.css') }}" media="all" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="{{ config('cabinet.url') }}/favicon.ico" />
    <script type="text/javascript">
        const CABINET_URL = "{{ config('cabinet.url') }}";
    </script>
</head>
<body class="scale-in-up-small mui-enter">
    <div id="page">
        <header id="header" class="row">
            <div class="large-12 columns ">
                <div class="top-bar cabinet">
                    <div class="large-12 column">
                        <a class="float-left animation-link" style="display: block;" href="{{ config('cabinet.url') }}">
                            <img src="{{ config('cabinet.url') }}/public/images/logotip_cabinet.svg" width="35" height="42" alt="">
                        </a>
                        <h1>{{ trans('labels.head.title') }}</h1>
                    </div>
                </div>
            </div>
        </header>
        <div class="row">
            <div class="small-12 column">
                <div id="top-sticky-container" data-sticky-container class="sticky-container" style="min-height: 29px;">
                    <div class="sticky small-12 column text-center min-height-info notification-sticky"
                         data-options="marginTop:0;"
                         data-sticky
                         data-top-anchor="header:bottom"
                         data-btm-anchor="footer:bottom"
                         data-sticky-on="small">
                        <div id="top-notify" class="notify">
                            <b class="alert"></b>
                            <b class="primary"></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Секція для двох верхніх блоків -->
        <section id="top-blocks" class="row" data-equalizer="top-blocks">
            @include('top-blocks.information')
            @include('top-blocks.user')
        </section>
        <div id="content-wrapper"></div>
        <footer id="footer" class="row">
            <div class="large-12 columns ">
                <div class="top-bar" style="margin-bottom:0;">
                    <div class="large-12 column none">
                        <small class="float-right">&copy; {{ trans('labels.footer.cis') }} {{date("Y")}}</small>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- Scripts -->
    <script src="{{ config('cabinet.url') }}/public/js/jquery.min.js" type="text/javascript"></script>
    <script src="{{ config('cabinet.url') }}/public/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="{{ config('cabinet.url') }}/public/js/foundation.min.js" type="text/javascript"></script>
    <script src="{{ config('cabinet.url') }}/public/js/what-input.min.js" type="text/javascript"></script>
    <script src="{{ config('cabinet.url') }}/public/js/jquery.scrollTo.min.js" type="text/javascript"></script>
    <script src="{{ config('cabinet.url') }}/public/js/cabinet.utils.min.js" type="text/javascript"></script>
    <script src="{{ config('cabinet.url') }}/resource/services/menu-services" type="text/javascript"></script>
    <script src="{{ mix('js/manifest.js') }}" type="text/javascript"></script>
    <script src="{{ mix('js/vendor.js') }}" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
</body>
</html>
