<section class="xmedium-6 column">
    <article class="callout margin-info-xmedium clearfix" data-equalizer-watch="top-blocks">
        <h5 class="callout hide-for-xmedium small cabinet margins minimize-padding max">
            <i></i>
            <span>{{ trans('labels.information.title') }}</span>
        </h5>
        <h5 class="callout small show-for-xmedium cabinet margins">{{ trans('labels.information.title') }}</h5>
        <div id="information" class="column small-12 show-for-xmedium">
            <p class="first-paragraph">
                {{ trans('labels.information.appeal') }}
            </p>
            <p>
                {{ trans('labels.head.description_shot') }}
            </p>
            <p>
                {{ trans('labels.information.description') }}
            </p>
        </div>
    </article>
</section>
