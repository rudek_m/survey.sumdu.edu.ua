<section class="xmedium-6 column">
    <div class="callout paddings margin-none" data-equalizer-watch="top-blocks" style="height: 276px;">
        <article class="m-h100">
            <h5 class="callout cabinet small two-icon">{{ trans('labels.user.title') }}</h5>
            <div class="small-12 column none">
                <h4 class="large-4 xmedium-2 medium-2 xxsmall-2 small-3 column none">
                    {{ Auth::user()->person['surname'] }} {{ Auth::user()->person['name'] }} {{ Auth::user()->person['patronymic'] }}
                </h4>
                <div id="avatar" class="large-4 xmedium-4 medium-3 xxsmall-4 xlsmall-5 ssmall-7 small-12 column none ssmall-text-right">
                    <div id="person-photo">
                        <img src="/photo" height="128" width="128" alt="">
                    </div>
                </div>
            </div>
        </article>
        <nav class="clearfix negative-m-t">
            <a href="{{ config('cabinet.url') }}" title="{{ trans('labels.user.main') }}" class="button m-t">{{ trans('labels.user.main') }}</a><button class="button m-t menu-services divider" data-toggle="menu-services">&nbsp;</button>
            <a href="/logout" title="{{ trans('labels.user.logout') }}" class="button m-t">{{ trans('labels.user.logout') }}</a>
        </nav>
    </div>
</section>
