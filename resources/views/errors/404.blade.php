<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="google" content="notranslate">
    <meta name="theme-color" content="#444e8b">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Cache-control" content="no-cache"/>
    <meta name="description" content="{{ trans('labels.head.description_shot') }}"/>
    <meta name="keywords" content="{{ trans('labels.head.keywords') }}"/>
    <title>{{ trans('labels.head.title') }}</title>

    <!-- Styles -->
    <link href="{{ config('cabinet.url') }}/public/css/cabinet.min.css" rel="stylesheet" type="text/css" media="all" />

    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="{{ config('cabinet.url') }}/favicon.ico" />
</head>
<body>
    <div id="page">
        <header class="row">
            <div class="large-12 columns">
                <div class="top-bar cabinet">
                    <div class="large-12 column">
                        <a class="float-left" style="display: block;" href="{{ config('cabinet.url') }}">
                            <img src="{{ config('cabinet.url') }}/public/images/logotip_cabinet.svg" width="35" height="42" alt="{{ trans('header.logo')  }}">
                        </a>
                        <h1>{{ trans('labels.head.title') }}</h1>
                    </div>
                </div>
            </div>
        </header>
        <main>
            <div class="row">
                <div id="top-notify" class="small-12 column text-center min-height-info notify">
                    <b class="alert"></b>
                    <b class="primary"></b>
                </div>
            </div>
            <div class="row">
                <div class="xmedium-6 small-12 column">
                    <h2 class="small-12 column primary xmedium-text-left lsmall-text-center">
                        <span class="size-72">404</span><br/>{{ trans('labels.404.title') }}
                    </h2>
                    <div class="small-12 column xmedium-text-left lsmall-text-center"><br/>
                        <a href="/" style="text-decoration: underline;"><small>{{ trans('labels.user.main') }}</small></a>
                    </div>
                </div>
                <div class="xmedium-6 small-12 column lsmall-text-center">
                    <img src="{{ config('cabinet.url') }}/public/images/404.png" width="402" height="413">
                </div>
            </div>
        </main>
    </div>
    <footer id="footer" class="row">
        <div class="large-12 columns ">
            <div class="top-bar" style="margin-bottom:0;">
                <div class="large-12 column none">
                    <small class="float-right">&copy; {{ trans('labels.footer.cis') }} {{date("Y")}}</small>
                </div>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="{{ config('cabinet.url') }}/public/js/jquery.min.js"></script>
    <script type="text/javascript" src="{{ config('cabinet.url') }}/public/js/foundation.min.js"></script>
    <script type="text/javascript" src="{{ config('cabinet.url') }}/public/js/what-input.min.js"></script>
</body>
</html>
