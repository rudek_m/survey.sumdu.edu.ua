<?php

return [
    'surveys.success' => 'Опитування успішно пройдено.',

    'cabinet.data_error' => 'Помилка отримання даних з особистого кабінету.',
    'cabinet.error_logout' => 'Помилка виходу з кабінету.',

    'survey.success_remove' => 'Опитування успішно видалено.'
];
