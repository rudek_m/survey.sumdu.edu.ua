<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ви не авторизовані.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'key_absent' => 'Відсутній токен авторизації.',
    'forbidden' => 'Не достатньо прав доступу до даного ресурсу.',
    'forbidden_group' => 'Ви не маєте доступу до даного ресурсу.'

];
