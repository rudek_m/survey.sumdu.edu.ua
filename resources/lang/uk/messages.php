<?php

return [
    'surveys.success' => 'Опитування успішно пройдено.',

    'cabinet.data_error' => 'Помилка отримання даних з особистого кабінету.',
    'cabinet.error_logout' => 'Помилка виходу з кабінету.',

    'survey.success_remove' => 'Опитування успішно видалено.',
    'survey.copied' => 'Опитування успішно скопійовано.',

    'contingent.success_remove' => 'Контингент успішно видалено.',

    'survey.not_active' => 'Опитування не активне.',
    'survey.passed' => 'Опитування вже пройдено.',
    'survey.completed' => 'Опитування завершено.',
    'survey.access_denied' => 'Ви не маєте доступу до даного опитування.',

    'survey.data.access' => 'Ви не маєте доступу до цих даних.',
    'csrf.mismatch' => 'Помилка підробки міжсайтового запиту.'
];
