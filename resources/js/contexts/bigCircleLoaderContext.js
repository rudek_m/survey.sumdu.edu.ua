import { createContext } from "react";

const BigCircleLoaderContext = createContext(null);

export default BigCircleLoaderContext;
