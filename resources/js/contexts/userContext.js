import { createContext } from 'react';

const userState = {
    guid: '',
    role: 'guest',
    isAdmin: false
};

const UserContext = createContext(userState);

export default UserContext;
