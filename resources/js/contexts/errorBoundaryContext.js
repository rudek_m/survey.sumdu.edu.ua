import {createContext} from "react";

const ErrorBoundaryContext = createContext(null);

export default ErrorBoundaryContext;
