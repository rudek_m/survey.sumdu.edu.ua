import {createContext} from 'react';

const initialContext = {
    message: '',
    type: 'primary'
};

const StickyMessageSurveyContext = createContext(initialContext);

export default StickyMessageSurveyContext;
