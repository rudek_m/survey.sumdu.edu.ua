import {useReducer, useEffect, useContext} from 'react';
import axios from 'axios';

import dataFetchReducer from '../../reducers/dataFetchReducer';
import { FETCH_INIT, FETCH_SUCCESS, FETCH_FAILURE, API_PREFIX } from '../../constants/dataFetch';
import errorBoundaryContext from "../../contexts/errorBoundaryContext";
import StickyMessageSurveyContext from "../../contexts/stickyMessageSurveyContext";

const initialState = {
  rawData: null,
  isLoading: true,
  isError: false
};


export default function useDataApi({ url, method = 'GET', params = {} }) {
    const [state, dispatch] = useReducer(dataFetchReducer, initialState);
    const { setErrorState } = useContext(errorBoundaryContext);
    const { setStickySurveyState } = useContext(StickyMessageSurveyContext);

    useEffect(() => {

        let ignore = false;
        dispatch({ type: FETCH_INIT });

        const fetchData = async () => {
            try {

                const result = await axios({
                    url: `${API_PREFIX}${url}`,
                    method,
                    params,
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                });

                if (!ignore) {
                    if (result.data.message) {
                        const type = result.data.status === 'success' ? 'primary' : 'alert';
                        setStickySurveyState({type: type, message: result.data.message});
                    }
                    dispatch({ type: FETCH_SUCCESS, payload: result.data.data })
                }
            } catch (error) {
                if (error.response) {
                    if (error.response.status === 403) {
                        setStickySurveyState({type: 'primary', message: ''});
                        setStickySurveyState({type: 'alert', message:  error.response.data.message});
                        dispatch({
                            type: FETCH_FAILURE,
                            payload: error.response
                        });
                    }
                    if ( [401, 404].includes(error.response.status) ) {
                        setErrorState({hasError: true, error: error.response});
                    } else {
                        setStickySurveyState({type: 'alert', message: error.response.data.message});
                        dispatch({
                            type: FETCH_FAILURE,
                            payload: error.response
                        });
                    }
                } else {
                    dispatch({ type: FETCH_FAILURE, payload: error.message })
                }
            }
        };

        fetchData();

        return () => (ignore = true)
    },[]);



    return state;
}
