import {useEffect} from "react";

const foundationReinit = () => {
    useEffect(() => {
        $('#survey-sticky-container div.sticky').foundation('_setSizes');
        $('.dropdown.menu').each(function() {
            let $this = $(this);

            if ( !$this.data('zfPlugin') ) {
                $this.foundation();
            }
        });
    });
};

export default foundationReinit;
