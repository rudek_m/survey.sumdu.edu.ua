const timeouts = {
    'alert': [],
    'primary': []
};

export function popupNotify( setState, type, message) {
    clearTimeout(timeouts[type]);
    setState(() => ({
        [type]: message,
        notify: true
    }));
    timeouts[type] = setTimeout(() =>
        setState(() =>({
            [type]: '',
            notify: false
        })),
    10000);
}
