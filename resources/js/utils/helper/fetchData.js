import axios from 'axios';

import { API_PREFIX } from '../../constants/dataFetch';

const fetchData = ({ url, method = 'GET', data = {}, params = {}, configs = {} }) => {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await axios({
                url: `${API_PREFIX}${url}`,
                method,
                data,
                params,
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                },
                ...configs
            });

            resolve(response)
        } catch (error) {
            reject(error)
        }
    });
};

export default fetchData
