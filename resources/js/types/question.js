import PropTypes from 'prop-types';
import variant from './variant'

const question = PropTypes.shape({
    id: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    id_survey: PropTypes.number,
    variants: PropTypes.arrayOf(variant)
});

const questionDefault = {
    id: 0,
    text: '',
    type: 'single',
    id_survey: 0,
    variants: []
};

export { question, questionDefault };
