import PropTypes from 'prop-types';

const contingent = PropTypes.shape({
    id: PropTypes.oneOfType([()=>null, PropTypes.number.isRequired]),
    id_categ1: PropTypes.oneOfType([()=>null, PropTypes.number.isRequired]),
    id_categ2: PropTypes.oneOfType([()=>null, PropTypes.number.isRequired]),
    id_course: PropTypes.oneOfType([()=>null, PropTypes.number.isRequired]),
    id_degree: PropTypes.oneOfType([()=>null, PropTypes.number.isRequired]),
    id_div: PropTypes.oneOfType([()=>null, PropTypes.number.isRequired]),
    id_group: PropTypes.oneOfType([()=>null, PropTypes.number.isRequired]),
    id_specialty: PropTypes.oneOfType([()=>null, PropTypes.number.isRequired]),
    id_survey: PropTypes.oneOfType([()=>null, PropTypes.number.isRequired]),
    id_form: PropTypes.oneOfType([()=>null, PropTypes.number.isRequired]),
    id_kod_std: PropTypes.oneOfType([()=>null, PropTypes.number.isRequired]),
    name_kod_std: PropTypes.string,
    name_form: PropTypes.string,
    name_specialty: PropTypes.string,
    name_degree: PropTypes.string,
    name_course: PropTypes.string,
    name_group: PropTypes.string,
    name_div: PropTypes.string,
    main_post: PropTypes.number,
    not_include_subdep: PropTypes.number
});

const contingentDefault = {
    id: null,
    id_categ1: null,
    id_categ2: null,
    id_course: null,
    id_degree: null,
    id_div: null,
    id_group: null,
    id_specialty: null,
    id_survey: null,
    id_form: null,
    id_kod_std: null,
    name_form: '',
    name_kod_std: '',
    name_specialty: '',
    name_degree: '',
    name_course: '',
    name_group: '',
    name_div: '',
    main_post: 0,
    not_include_subdep: 0
};

export { contingent, contingentDefault };
