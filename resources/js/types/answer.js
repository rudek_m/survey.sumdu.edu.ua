import PropTypes from 'prop-types';

const answer = PropTypes.shape({
    id_var: PropTypes.number.isRequired,
    id_ques: PropTypes.number.isRequired,
    id_survey: PropTypes.number.isRequired,
    text: PropTypes.oneOfType([()=>null, PropTypes.string.isRequired])
});

export default answer;
