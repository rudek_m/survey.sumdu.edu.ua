import PropTypes from 'prop-types';
import answer from './answer';

const variant = PropTypes.shape({
    id: PropTypes.number.isRequired,
    text: PropTypes.oneOfType([()=>null, PropTypes.string.isRequired]),
    id_ques: PropTypes.number,
    answers: PropTypes.arrayOf(answer)
});

export default variant;
