import PropTypes, {bool} from 'prop-types';
import { question } from './question'

const survey = PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    from: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
    status: PropTypes.number.isRequired,
    status_name: PropTypes.string.isRequired,
    questions: PropTypes.arrayOf(question),
    answers_exist: PropTypes.number,
    respondents_count: PropTypes.number,
    answers_count: PropTypes.number,
    is_accessible: PropTypes.number
});

const surveyDefault = {
    id: 0,
    title: '',
    from: new Date().toDateString(),
    to: new Date().toDateString(),
    status: 1,
    status_name: '',
    answers_exist: 0,
    respondents_count: 0,
    answers_count: 0,
    questions: [],
    contingents: [],
    is_accessible: 0
};

export { survey, surveyDefault };
