import React, { Component } from 'react';
import { injectIntl } from 'react-intl';

import StickyMessage from "../Sticky/StickyMessage";
import ErrorBoundaryContext from "../../contexts/errorBoundaryContext";

class ErrorBoundary extends Component {

    constructor(props) {
        super(props);
        this.state = { hasError: false, error: null, info: null };
        this.setState = this.setState.bind(this);
    }

    static getDerivedStateFromError(error, info) {
        return { hasError: true, error, info }
    }

    componentDidCatch(error, info) {
        console.log(info);
        console.log(error);
    }

    render() {
        const { intl } = this.props;
        if (this.state.hasError) {
            if (this.state.error.status === 401) {
                window.location = `${CABINET_URL}?error=${sv.utf8ToWin1251Urlencoded(this.state.error.data.message)}`;
                return '';
            }
            if (this.state.error.status === 404 ) {
                window.location = '/404';
                return '';
            }

            return (
                <StickyMessage type={"alert"} message={`${intl.formatMessage({id: 'errors.error'})}: ${this.state.error.message}`}/>
            )
        }

        return (
            <ErrorBoundaryContext.Provider value={{setErrorState: this.setState}}>
                {this.props.children}
            </ErrorBoundaryContext.Provider>
        );
    }
}

export default injectIntl(ErrorBoundary);
