import React, {useLayoutEffect, useRef, useContext} from 'react';

import StickyMessageSurveyContext from "../../contexts/stickyMessageSurveyContext";

const StickyMessage = ({type, message}) => {
    const { stickySurveyState } = useContext(StickyMessageSurveyContext);
    const sticky = useRef();

    if ( !type && !message) {
        type = stickySurveyState.type;
        message = stickySurveyState.message;
    }

    useLayoutEffect(() => {
        const $sticky = $(sticky.current);

        if ( !$sticky.data('resize') ) {
            $sticky.foundation();
            utils.onMessageStickyStuck();
        }

        message && utils.showMessage($sticky, type, message);
        $sticky.foundation('_setSizes');
    });

    return (
        <div className="row">
            <div className="small-12 column">
                <div id="survey-sticky-container" data-sticky-container className="sticky-container" style={{minHeight: "29px"}}>
                    <div className="sticky small-12 column text-center min-height-info notification-sticky"
                         data-options="marginTop:0;" data-sticky data-top-anchor="top-blocks:bottom"
                         data-btm-anchor="footer:bottom" data-sticky-on="small" style={{maxWidth: '1165px'}} ref={sticky}>
                        <div id="survey-notify" className="notify">
                            <b className="alert"/>{" "}<b className="primary"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default StickyMessage;
