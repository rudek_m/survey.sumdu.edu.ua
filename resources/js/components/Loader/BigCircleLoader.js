import React, { useState } from 'react';

import BigCircleLoaderContext from "../../contexts/bigCircleLoaderContext";

const BigCircleLoader = ( {children} ) => {
    const [bigCircleLoaderState, setBigCircleLoaderState] = useState(false);

    /*let id = 0;
    useEffect(() => {
        id && clearTimeout(id);

        if ( bigCircleLoaderState ) {
            id = setTimeout(() => {

            }, 500)
        }
    },[bigCircleLoaderState]);*/

    return (
        <BigCircleLoaderContext.Provider value={{setBigCircleLoaderState}}>
            <>
                <div id="waitingbg" style={{display: (bigCircleLoaderState ? 'block':'none')}}/>
                <div id="circularG" style={{display: (bigCircleLoaderState ? 'block':'none')}}>
                    <div id="circularG_1" className="circularG"/>
                    <div id="circularG_2" className="circularG"/>
                    <div id="circularG_3" className="circularG"/>
                    <div id="circularG_4" className="circularG"/>
                    <div id="circularG_5" className="circularG"/>
                    <div id="circularG_6" className="circularG"/>
                    <div id="circularG_7" className="circularG"/>
                    <div id="circularG_8" className="circularG"/>
                </div>
                {children}
            </>
        </BigCircleLoaderContext.Provider>
    );
};

export default BigCircleLoader;


