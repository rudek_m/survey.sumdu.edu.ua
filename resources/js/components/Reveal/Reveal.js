import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Reveal extends Component {

    constructor(props) {
        super(props);
        this.revealRef = React.createRef();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.open !== this.props.open) {
            if (this.props.open) {
                this.$reveal.foundation('open');
            } else {
                this.$reveal.foundation('close');
            }
        }
    }

    componentDidMount() {
        this.$reveal = $(this.revealRef.current);
        this.$reveal.foundation();
        if (this.props.open) {
            this.$reveal.foundation('open');
        }
    }

    componentWillUnmount() {
        if (this.$reveal) {
            this.$reveal.foundation('close').foundation('destroy').remove();
            Foundation.IHearYou();
        }
    }

    render() {
        return (
            <div id={this.props.idReveal} ref={this.revealRef} className={`reveal padding-top-bottom modal-setting ${this.props.classes}`} data-reveal={true} data-close-on-click={false} data-reset-on-close={false} data-close-on-esc={false}>
                {this.props.children}
            </div>
        )
    }
}

Reveal.propTypes = {
    idReveal: PropTypes.string.isRequired,
    revealRef: PropTypes.object,
    classes: PropTypes.string,
    open: PropTypes.bool.isRequired
};

Reveal.defaultProps = {
    classes: '',
    open: false
};

export default Reveal;
