import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Reveal from "../Reveal";

class RevealForm extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Reveal idReveal={this.props.idReveal} classes={this.props.classes} open={this.props.open}>
                <form className="clearfix" onSubmit={this.props.onSubmit}>
                    {this.props.children}
                </form>
            </Reveal>
        )
    }
}

RevealForm.propTypes = {
    idReveal: PropTypes.string.isRequired,
    onSubmit: PropTypes.func,
    open: PropTypes.bool
};

RevealForm.defaultProps = {
    classes: 'paddings'
};

export default RevealForm;
