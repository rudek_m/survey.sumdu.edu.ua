import React from 'react';
import {FormattedMessage} from "react-intl";
import Reveal from "../Reveal";
import PropTypes from 'prop-types';
import {injectIntl, IntlProvider} from 'react-intl'


const RevealRemove = ({intl, id, open, type, title, onClose, onRemove}) => {

    return (
        <Reveal idReveal={id} open={open}>
            <h5 className="callout cabinet small margin-left-bottom-right">
                <button className="close-button"  aria-label="Close reveal" type="button" onClick={onClose}>
                    <span aria-hidden="true">&times;</span>
                </button>
                {intl.formatMessage({id: 'remove.title'})}
            </h5>
            <div className="small-12 column">
                <p className="first-paragraph">
                    {type &&
                        <FormattedMessage
                            id={`remove.paragraph.${type}`}
                            values={{[type]:  <span className="fw-bold">«{title}»</span>}}
                        />
                    }
                </p>
                <p className="question margin-bottom-none">
                    {type && <FormattedMessage id={`remove.question.${type}`} />}
                </p>
                <a id="remove" className="button m-t" onClick={onRemove}>{intl.formatMessage({id: 'remove'})}</a>{" "}
                <button className="button close m-t hollow" type="button" onClick={onClose} title={intl.formatMessage({id: 'cancel'})}>
                    {intl.formatMessage({id: 'cancel'})}
                </button>
            </div>
        </Reveal>
    )
};

RevealRemove.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}).isRequired,
    id: PropTypes.string,
    open: PropTypes.bool.isRequired,
    type: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired
};

RevealRemove.defaultProps = {
    id: '',
    open: false,
    type: '',
    title: ''
};

export default injectIntl(RevealRemove);
