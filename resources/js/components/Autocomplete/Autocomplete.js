import React, {Component} from 'react';
import PropTypes from "prop-types";
import {FormattedMessage, injectIntl, IntlProvider} from "react-intl";
import fetchData from "../../utils/helper/fetchData";
import ErrorBoundaryContext from "../../contexts/errorBoundaryContext";


class Autocomplete extends Component {
    constructor(props) {
        super(props);
        this.inputRef = React.createRef();
        this.hiddenRef = React.createRef();

        this.state = {
            value: {
                [`id_${props.name}`]: this.props.idValue,
                [`name_${props.name}`]: this.props.nameValue
            }
        };

        this.changeNameValue = this.changeNameValue.bind(this);
    }

    static getDerivedStateFromProps(props, state) {
        if (props.idValue !== state.value[`id_${props.name}`]) {
            return {
                value: {
                    [`id_${props.name}`]: props.idValue,
                    [`name_${props.name}`]: props.nameValue
                }
            }
        }
        return null;
    }

    componentDidMount() {
        const $autocomplete = $(this.inputRef.current);

        const nameValueElement = this.inputRef.current;

        const _this = this;

        const context = this.context;

        const autocomplete = $autocomplete.autocomplete({
            minLength: 0,
            source: async function(request, response) {
                try {
                    const { data: { data } } = await fetchData({ url: _this.props.url, params: {search: request.term} });
                    response(data);
                } catch ( error ) {
                    const errorState = {hasError: true, title: _this.props.intl.formatMessage({id: 'errors.ajax'})};
                    if (error.response) {
                        if ([401, 404].includes(error.response.status)) {
                            context.setErrorState({hasError: true, error: error.response});
                        } else {
                            if (error.response) {
                                errorState.title = error.response.data.message;
                            }
                        }
                    }
                    response([errorState]);
                }
            },
            select: function(event, ui) {
                if (ui.item.id) {
                    const state = {
                        value: {
                            [`id_${_this.props.name}`]: ui.item.id,
                            [`name_${_this.props.name}`]: ui.item.name
                        }
                    };
                    _this.setState(() => state);
                    _this.props.changeAutocomplete(state.value);
                }
                return false;
            },
            focus: function(event, ui) {
                if (ui.item.id && event.keyCode !== undefined) {
                    const state = {
                        value: {
                            [`id_${_this.props.name}`]: ui.item.id,
                            [`name_${_this.props.name}`]: ui.item.name
                        }
                    };
                    _this.setState(() => state);
                    _this.props.changeAutocomplete(state.value);
                }
                return false;
            },
            close: function(event) {
                event.stopPropagation();
                if (event.keyCode === 27) {
                    _this.props.changeAutocomplete(_this.state.value)
                }
            }
        }).autocomplete('instance');

        autocomplete._renderItem = function (ul, item) {
            let itemAutocomplete = `<li class="ui-autocomplete-notfound"><div>${_this.props.intl.formatMessage({id: `contingent.not_found.${_this.props.name}`})}</div></li>`;
            if (item.hasError) {
                itemAutocomplete = `<li class="ui-autocomplete-notfound alert"><div>${item.title}</div></li>`;
            }

            if ( item.id !== undefined ) {
                itemAutocomplete = $('<li class="ui-autocomplete-item"/>').append(`<div><span class="l-h-1-1 d-b" style="margin:2px 0">${item.name}</span></div>`);
            }

            return $(itemAutocomplete).appendTo(ul);
        };
        autocomplete._renderMenu = function (ul, items) {
            var that = this;
            $.each(items, function (index, item) {
                that._renderItemData(ul, item);
            });
            ul.addClass('ui-autocomplete-custom-menu');
        };
        autocomplete._resizeMenu = function () {
            const autocompleteMenu = this.menu.element;
            const autocompleteInput = this.element;
            autocompleteMenu.outerWidth(0);
            autocompleteMenu.outerWidth(autocompleteInput.outerWidth());
            autocompleteMenu.offset({top: autocompleteInput.offset().top + autocompleteInput.outerHeight(), left: autocompleteInput.offset().left});
        };

        $autocomplete.blur(function(e){
             if ( nameValueElement.value === '' ) {
                 _this.props.changeAutocomplete({
                     [`id_${_this.props.name}`]: null,
                     [`name_${_this.props.name}`]: ''
                 })
             }
             if ( nameValueElement.value !== '' ) {
                 _this.props.changeAutocomplete(_this.state.value);
             }
        });

        $autocomplete.focus(function() {
            $(this).select();
            autocomplete.search();
        });
    }

    changeNameValue(e) {
        this.props.changeAutocomplete({[`name_${this.props.name}`]: e.target.value});
    }

    render() {
        return (
            <>
                <label htmlFor={`${this.props.idPrefix}-search`}>
                    <FormattedMessage id={this.props.label} />
                </label>
                <input id={`${this.props.idPrefix}-search`}
                       className={this.props.inputClass}
                       type="text"
                       ref={this.inputRef}
                       name={`name_${this.props.name}`}
                       value={this.props.nameValue || ''}
                       onChange={this.changeNameValue}
                       disabled={this.props.disabled}
                />
                <input
                    id={`${this.props.idPrefix}-search-id`}
                    type="hidden"
                    ref={this.hiddenRef}
                    name={`id_${this.props.name}`}
                    defaultValue={this.props.idValue}
                />
            </>
        )
    }
}

Autocomplete.contextType = ErrorBoundaryContext;

Autocomplete.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}).isRequired,
    idPrefix: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    idValue: PropTypes.oneOfType([()=>null, PropTypes.number.isRequired]),
    nameValue: PropTypes.oneOfType([()=>null, PropTypes.string.isRequired]),
    changeAutocomplete: PropTypes.func.isRequired,
    inputClass: PropTypes.string,
    disabled: PropTypes.bool
};

Autocomplete.defaultProps = {
    inputClass: ''
};

export default injectIntl(Autocomplete);



