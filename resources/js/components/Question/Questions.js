import React from 'react';

import Question from "./Question/Question";

const Questions = ({questions, isChangeableSurvey}) => {

    return (
        <ol className="questions">
            {questions.map((question, index) => <Question key={index} index={index} question={question} isChangeableSurvey={isChangeableSurvey}/>)}
        </ol>
    );
};

export default Questions;
