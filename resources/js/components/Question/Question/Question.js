import React, {useState} from 'react';

import Variants from "../../Variant/Variants";

const Question = ({question: {id, text, variants, type}, index, isChangeableSurvey}) => {

    const [textAnswer, setTextAnswer] = useState('');

    const changeText = (event) => {
        setTextAnswer(event.target.value);
    };

    return (
        <li className={`callout paddings${index >= 9 ? ' more-9' : ''}`}>
            <p>{text}</p>
            {['single', 'multi'].includes(type) ?
                <Variants variants={variants} type={type} isChangeableSurvey={isChangeableSurvey}/> :
                <div className="textarea-wrapper">
                    {isChangeableSurvey ?
                        <>
                            <textarea className="margin-none" name={`questions[${id}][text]`} value={textAnswer} onChange={changeText}/>
                            <input type="hidden" name={`questions[${id}][id_var]`} value={variants[0]['id']}/>
                        </> :
                        <textarea className="margin-none" value={variants[0].answers.length ? variants[0].answers[0].text : ''} readOnly={true}/>}
                </div>
            }
        </li>
    );
};



export default Question;
