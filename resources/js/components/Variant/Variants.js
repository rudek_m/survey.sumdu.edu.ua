import React from 'react';

import Variant from "./Variant/Variant";

const Variants = ({variants, type, isChangeableSurvey}) => {
    return (
        <ul className="answers">
           {variants.map((variant, index) => <Variant key={index} variant={variant} type={type} isChangeableSurvey={isChangeableSurvey}/>)}
        </ul>
    )
};

export default Variants;
