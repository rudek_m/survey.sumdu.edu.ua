import React from 'react';

const Variant = ({variant, type, isChangeableSurvey}) => {
    const inputType = {multi: "checkbox", single: "radio"};
    const id = `v${variant.id_ques}${variant.id}`;

    const onKeyDownHandle = (event) => {
        const key = event.which || event.keyCode;
        if (key === 32 || key === 13) {
            event.preventDefault();
            event.target.click();
        }
    };

    return (
        <li>
            {isChangeableSurvey ?
                <>
                    <input id={id}
                         type={inputType[type]}
                         name={`questions[${variant.id_ques}][]`}
                         value={`${variant.id}`}
                         defaultChecked={!!variant.answers.length}/>
                    <label htmlFor={id} className={`md-${inputType[type]}`} tabIndex="0" onKeyDown={onKeyDownHandle}>{variant.text}</label>
                </> :
                <span className={`md-${inputType[type]}${!!variant.answers.length ? ' checked' : ''}`} style={{fontWeight:'normal'}}>{variant.text}</span>
            }
        </li>
    )
};

export default Variant;
