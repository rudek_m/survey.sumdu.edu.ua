import React from 'react';
import PropTypes from 'prop-types';
import {FormattedMessage} from "react-intl";

const CheckboxRadio = ({id, type, view, name, value, checked, label, labelText, changeHandler, disabled = false}) => {
    view = view || type;

    const keyDownLabel = e => {
        const key = e.which || e.keyCode;
        if ( key === 32 || key === 13) {
            e.preventDefault();
            e.target.click();
        }
    };

    return (
        disabled ?
        <span className={`md-${type}`}>{labelText ? labelText : <FormattedMessage id={label}/>}</span> :
        <div className="custom-checkbox-radio">
            <input id={id} type={type} name={name} value={value} onChange={changeHandler} checked={checked} />
            <label className={`md-${view}`} htmlFor={id} tabIndex="0" onKeyDown={keyDownLabel}>{labelText ? labelText : <FormattedMessage id={label}/>}</label>
        </div>
    );
};

CheckboxRadio.propTypes = {
    id: PropTypes.string,
    type: PropTypes.string,
    view: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.string,
    checked: PropTypes.bool,
    label: PropTypes.string,
    changeHandler: PropTypes.func
};

CheckboxRadio.defaultProps = {
    id: null,
    type: 'radio',
    name: null,
    value: null,
    checked: false,
    changeHandler: null
};

export default CheckboxRadio;
