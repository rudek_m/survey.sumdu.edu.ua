import React, { Component } from 'react';
import {injectIntl, IntlProvider} from "react-intl";
import PropTypes from "prop-types";

class DatePicker extends Component {

    constructor(props) {
        super(props);
        this.intl = props.intl;
        this.inputRef = this.props.inputRef();
    }

    componentDidMount() {
        const input = this.props.inputRef();
        const classDiv = this.props.classDiv;
        const handleChange = this.props.handleChange;
        $(input).datepicker(Object.assign({
            dateFormat : 'dd.mm.yy',
            beforeShow : function(input, inst) {
                $('#ui-datepicker-div').addClass('custom-ui-calendar');
                //console.log('datepicker');
                //var t = this;
                /*t.outerWidth(0);
                t.outerWidth(this.element.outerWidth() + $('#' + selector_prefix + '-search-icon').outerWidth());
                t.offset({top: this.element.offset().top + this.element.outerHeight(), left: this.element.offset().left});*/
            },
            onSelect: function(dateText,inst) {
                inst.inline = false;
                handleChange({[classDiv]: dateText});
            }
        }, this.props.configs))
    }

    componentWillUnmount() {
        const $input = $(this.props.inputRef());
        if ($input.datepicker()) {
            $input.datepicker('hide');
            $input.datepicker('destroy');
            $input.remove();
        }
    }

    render() {
        return (
            <div className={`small-12 xxxlsmall-6 column m-b ${this.props.classDiv}`} data-placeholder={!!this.props.datePlaceholder && this.intl.formatMessage({id: this.props.datePlaceholder})}>
                <input className="margin-none" type="text"
                    name={this.props.name}
                    ref={this.inputRef}
                    value={this.props.date}
                    autoComplete="off"
                    readOnly={true}
                />
            </div>
        )
    }
}

DatePicker.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}).isRequired,
    name: PropTypes.string.isRequired,
    inputRef: PropTypes.func.isRequired,
    handleChange:PropTypes.func.isRequired,
    classDiv: PropTypes.string,
    date: PropTypes.string,
    datePlaceholder: PropTypes.string,
    configs: PropTypes.object
};

DatePicker.defaultProps = {
    classDiv: '',
    date: '',
    datePlaceholder: '',
    configs : {}
};

export default injectIntl(DatePicker);
