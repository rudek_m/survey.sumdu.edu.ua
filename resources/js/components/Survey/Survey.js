import React from 'react';
import { useParams } from 'react-router-dom';
import {CSSTransition} from "react-transition-group";

import useDataApi from "../../utils/hooks/useDataApi";
import ThreeCircleLoader from "../Loader/ThreeCircleLoader";
import foundationReinit from "../../utils/hooks/foundationReinit";
import SurveyForm from "./SurveyForm/SurveyForm";
import {surveyDefault} from "../../types/survey";

const Survey = () => {
    const { id } = useParams();
    const { rawData: surveyRawData, isLoading: isSurveyLoading, isError: isSurveyError } = useDataApi({
        url: `/surveys/${id}`,
        params: {data: 'answers'}
    });

    foundationReinit();

    const survey = surveyRawData && !isSurveyError ? surveyRawData : surveyDefault;

    return  <>
        {isSurveyLoading && <ThreeCircleLoader />}
        <CSSTransition in={!isSurveyLoading} timeout={300} appear={true} exit={false} classNames="tab">
            <div className={`tab ${isSurveyLoading ? 'tab-exit-done' : ''}`}>
                <SurveyForm survey={survey} />
            </div>
        </CSSTransition>
    </>;
};

export default Survey;



