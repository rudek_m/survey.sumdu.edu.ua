import React, {useContext} from 'react';
import {Link, useParams} from 'react-router-dom';
import {FormattedMessage, injectIntl} from "react-intl";

import useDataApi from "../../../utils/hooks/useDataApi";
import ThreeCircleLoader from "../../Loader/ThreeCircleLoader";
import foundationReinit from "../../../utils/hooks/foundationReinit";
import fetchData from "../../../utils/helper/fetchData";
import StickyMessageSurveyContext from "../../../contexts/stickyMessageSurveyContext";
import BigCircleLoaderContext from "../../../contexts/bigCircleLoaderContext";
import ErrorBoundaryContext from "../../../contexts/errorBoundaryContext";
import {CSSTransition} from "react-transition-group";

const SurveyResult = ({intl}) => {
    const { id } = useParams();
    const { setStickySurveyState } = useContext(StickyMessageSurveyContext);
    const { setBigCircleLoaderState } = useContext(BigCircleLoaderContext);
    const { setErrorState } = useContext(ErrorBoundaryContext);

    const { rawData: surveyRawData, isLoading: isSurveyLoading, isError: isSurveyError } = useDataApi({
        url: `/surveys/${id}`,
        params: {data: 'result'}
    });

    foundationReinit();

    const survey = surveyRawData && !isSurveyError ? surveyRawData : {title: null, questions: []};

    const resultSurveyClick = async (e) => {
        e.preventDefault();
        try {
            setBigCircleLoaderState(true);
            const { data } = await fetchData({
                url: `/surveys/result/export/${id}`,
                configs: {
                    responseType: 'arraybuffer'
                }
            });

            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(new Blob([data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'}));
            a.download = `${survey.title}.xlsx`;
            document.body.appendChild(a);
            a.click();
            a.parentNode.removeChild(a);
        } catch (error) {
            if ([401, 404].includes(error.response.status)) {
                setErrorState({hasError: true, error: error.response});
            } else {
                const errorState = {type: 'alert', message: intl.formatMessage({id: 'errors.ajax'})};
                if (error.response) {
                    errorState.message = error.response.data.message;
                }
                setStickySurveyState(errorState);
            }
        } finally {
            setBigCircleLoaderState(false);
        }
    };

    return <>
        {isSurveyLoading && <ThreeCircleLoader />}
        <CSSTransition in={!isSurveyLoading} timeout={300} appear={true} exit={false} classNames="tab">
            <div className={`clearfix ${isSurveyLoading ? 'tab-exit-done' : ''}`}>
                <div className="small-12 column">
                    <div className="callout clearfix padding-top-bottom">
                        <h5 className="callout cabinet small margin-left-bottom-right clearfix two-icon" style={{marginBottom: '32px'}}>
                            <FormattedMessage id="result.result"/>
                            <div className="h-icons">
                                {!!survey.questions.length &&
                                <>
                                    <a href={`/surveys/result/export/${survey.id}`} className="font-awesome size-21 va-top"
                                       onClick={resultSurveyClick}
                                       title={intl.formatMessage({id: 'result.result'})}>&#xf1c3;</a>&nbsp;&nbsp;
                                </>}
                                <Link to="/" className="surveys" title={intl.formatMessage({id: 'survey.title'})} />
                            </div>
                        </h5>
                        <div className="survey report-survey">
                            <div className="callout margin-left-bottom-right theme padding-top clearfix">
                                <div className="legend"><FormattedMessage id={`survey.settings.survey`}/></div>
                                <div className="small-12 column">
                                    <h6 className=" m-b">{survey.title}</h6>
                                    <p className="margin-bottom-half">
                                        <b>{intl.formatMessage({id: 'survey.settings.period'})}</b>{" "}
                                        {intl.formatMessage({id: 'survey.settings.from'})}&nbsp;{intl.formatDate(survey.from)}&nbsp;{intl.formatMessage({id: 'survey.settings.to'})}&nbsp;{intl.formatDate(survey.to)}
                                    </p>
                                    <p className="margin-bottom-half">
                                        <b><FormattedMessage id="survey.settings.status"/></b> {survey.status_name}{new Date(survey.to) < new Date().setHours(0,0,0, 0) ? `, ${intl.formatMessage({id: "survey.status.4"})}` : ''}
                                    </p>
                                    <p>
                                        <b><FormattedMessage id="result.total.respondents"/></b> {survey.respondents_count}
                                    </p>
                                </div>
                            </div>
                            {!survey.questions.length ?
                                <p className="primary text-center fw-bold size-12 margin-bottom-none">{intl.formatMessage({id: 'survey.empty_questions'})}</p> :
                                <div className="small-12 column">
                                    <table className="hover custom size-1em m-b-n l-h-1-1 p05">
                                        <colgroup>
                                            <col width="20px"/>
                                            <col/>
                                            <col/>
                                        </colgroup>
                                        <tbody>
                                        <>
                                            {survey.questions.map((question, index) => (
                                                <React.Fragment key={index}>
                                                    <tr>
                                                        <td colSpan="3"
                                                            className="fw-bold">{`${index + 1}. ${question.text}`}</td>
                                                    </tr>
                                                    {question.variants.map((variant, index) => (
                                                        <tr key={index} className="b-none">
                                                            <td className="border-top border-bottom"/>
                                                            <td className="border-right border-bottom">
                                                                {['single', 'multi'].includes(question.type) ?
                                                                    <>
                                                                        <span
                                                                            className={`md-${question.type === 'single' ? 'radio' : 'checkbox'}${variant.answers_count ? ' checked' : ''}`}
                                                                            style={{display: 'inline'}} />
                                                                        {variant.text}
                                                                    </> :
                                                                    <FormattedMessage id="result.open_question" />
                                                                }
                                                            </td>
                                                            <td className="border-right border-bottom text-center">{variant.answers_count}</td>
                                                        </tr>)
                                                    )}
                                                </React.Fragment>
                                            ))}
                                        </>
                                        </tbody>
                                    </table>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </CSSTransition>
    </>
};

export default injectIntl(SurveyResult);



