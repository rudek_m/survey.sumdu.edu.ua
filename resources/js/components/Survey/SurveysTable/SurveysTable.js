import React, {useContext, useEffect, useState} from 'react';
import {FormattedMessage, injectIntl, IntlProvider} from 'react-intl';
import {TransitionGroup, CSSTransition} from "react-transition-group";
import PropTypes from "prop-types";

import SurveyRow from "../SurveyRow/SurveyRow";
import StickyMessageSurveyContext from "../../../contexts/stickyMessageSurveyContext";
import errorBoundaryContext from "../../../contexts/errorBoundaryContext";
import foundationReinit from "../../../utils/hooks/foundationReinit";
import fetchData from "../../../utils/helper/fetchData";
import RevealRemove from "../../Reveal/RevealRemove/RevealRemove";
import { survey } from "../../../types/survey";
import BigCircleLoaderContext from "../../../contexts/bigCircleLoaderContext";

const initialRemoveReveal = {
    open: false,
    type: '',
    data: {
        title: ''
    }
};

const SurveysTable = ({intl, surveys, isAdmin}) => {
    const [ surveysState, setSurveysState ] = useState(surveys);
    const { setStickySurveyState } = useContext(StickyMessageSurveyContext);
    const { setBigCircleLoaderState } = useContext(BigCircleLoaderContext);
    const { setErrorState } = useContext(errorBoundaryContext);
    const [ removeRevealState, removeRevealSwitch ] = useState(initialRemoveReveal);

    foundationReinit();

    useEffect(() => {
        setSurveysState(surveys);
    }, [surveys.length]);

    const openRemoveReveal = ({data}) => {
        removeRevealSwitch({data, open: true, type: 'survey'});
    };

    const closeRemoveReveal = () => {
        removeRevealSwitch(initialRemoveReveal);
    };

    const removeSurveyHandler = async (e) => {
        e.preventDefault();
        try {
            const url = `/surveys/${removeRevealState.data.id}?_method=DELETE`;
            const { data: {message, status, data} } = await fetchData({ url, method:'POST'});
            if (status === 'success' && data === true) {
                setSurveysState(surveysState.filter(survey => survey.id !== removeRevealState.data.id));
                setStickySurveyState({type:'primary', message});
            } else {
                setStickySurveyState({type:'alert', message: intl.formatMessage({id: 'survey.error.remove'})});
            }
        } catch (error) {
            const errorState = {type: 'alert', message: intl.formatMessage({id: 'errors.ajax'})};
            if (error.response) {
                if ([401, 404].includes(error.response.status)) {
                    setErrorState({hasError: true, error: error.response});
                } else {
                    if (error.response) {
                        errorState.message = error.response.data.message;
                    }
                }
            }
            setStickySurveyState(errorState);
        } finally {
            closeRemoveReveal();
        }
    };

    const copySurveyHandler = async id => {
        try {
            setBigCircleLoaderState(true);
            const { data: {message, data} } = await fetchData({ url: `/surveys/copy/${id}`, method: 'POST'});
            setStickySurveyState({type:'primary', message});
            setSurveysState([...surveysState, data]);
        } catch (error) {
            const errorState = {type: 'alert', message: intl.formatMessage({id: 'errors.ajax'})};
            if (error.response) {
                if ([401, 404].includes(error.response.status)) {
                    setErrorState({hasError: true, error: error.response});
                } else {
                    if (error.response) {
                        errorState.message = error.response.data.message;
                    }
                }
            }
            setStickySurveyState(errorState);
        } finally {
            setBigCircleLoaderState(false);
        }
    };

    return (
        <>
            <h5 className={`callout cabinet small clearfix one-icon ${surveysState.length ? '' : 'margin-none'}`}>
                <FormattedMessage id={`surveys.title${surveysState.length ? '' : '.empty'}`}/>
            </h5>
            <div className="table-survey">
                {!!surveysState.length &&
                <div className="table-scroll">
                        <table className="hover custom size-1em m-b-n l-h-1-1 p05">
                            <thead>
                            <tr>
                                <th className="text-center"><FormattedMessage id="surveys.table.number.symbol"/></th>
                                <th className="text-center"><FormattedMessage id="surveys.table.title"/></th>
                                <th className="text-center"><FormattedMessage id="surveys.table.period"/></th>
                                <th className="text-center"><FormattedMessage id="surveys.table.status"/></th>
                                {isAdmin ? <th className="text-center"/> : null}
                            </tr>
                            </thead>
                            <TransitionGroup component="tbody">
                                {surveysState.map((survey, index) => (
                                        <CSSTransition key={survey.id} classNames="item" timeout={500}>
                                            <SurveyRow
                                                key={survey.id}
                                                survey={survey}
                                                index={index}
                                                isAdmin={isAdmin}
                                                removeSurveyClick={openRemoveReveal}
                                                copySurveyClick={copySurveyHandler}
                                            />
                                        </CSSTransition>
                                    )
                                )}
                            </TransitionGroup>
                        </table>
                    </div>
                }
            </div>
            {isAdmin ?
                <RevealRemove
                    open={removeRevealState.open}
                    type={removeRevealState.type}
                    title={removeRevealState.data.title}
                    onClose={closeRemoveReveal}
                    onRemove={removeSurveyHandler}
                /> :
                null
            }
        </>
    );
};

SurveysTable.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}).isRequired,
    surveys: PropTypes.arrayOf(survey).isRequired,
    isAdmin: PropTypes.bool.isRequired
};

export default injectIntl(SurveysTable);

