import React, { useContext } from 'react';
import {useParams, useHistory, Link} from 'react-router-dom';
import { FormattedMessage, injectIntl, IntlProvider } from "react-intl";
import PropTypes from 'prop-types';

import Questions from "../../Question/Questions";
import fetchData from "../../../utils/helper/fetchData";
import StickyMessageSurveyContext from "../../../contexts/stickyMessageSurveyContext";
import BigCircleLoaderContext from "../../../contexts/bigCircleLoaderContext";
import ErrorBoundaryContext from "../../../contexts/errorBoundaryContext";
import {survey, surveyDefault} from "../../../types/survey";

const SurveyForm = ({intl, survey}) => {
    const { id } = useParams();
    const { setStickySurveyState } = useContext(StickyMessageSurveyContext);
    const { setBigCircleLoaderState } = useContext(BigCircleLoaderContext);
    const { setErrorState } = useContext(ErrorBoundaryContext);
    const history = useHistory();

    const isChangeableSurvey = !survey.answers_exist && new Date(survey.to) >= new Date().setHours(0,0,0, 0);
    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const data = new FormData(e.target);
            setBigCircleLoaderState(true);
            const { data: {message, status} } = await fetchData({ url: `/answers/survey/${id}`, method: 'POST', data });
            if (status === 'success') {
                history.push('/');
                setStickySurveyState({type: 'alert', message: ' '});
                setStickySurveyState({type: 'primary', message});
            } else if ( status === 'error') {
                setStickySurveyState({type:'alert', message});
            }
        } catch (error) {
            if ([401, 404].includes(error.response.status)) {
                setErrorState({hasError: true, error: error.response});
            } else {
                const errorState = {type: 'alert', message: intl.formatMessage({id: 'errors.ajax'})};
                if (error.response) {
                    errorState.message = error.response.data.message;
                }
                setStickySurveyState(errorState);
            }
        } finally {
            setBigCircleLoaderState(false);
        }
    };

    return (
        <div className="small-12 column">
            <div className="callout clearfix paddings">
                <h5 className="callout cabinet small clearfix one-icon">
                    <FormattedMessage id="survey.title"/>
                    <div className="h-icons">
                        <Link to="/" className="surveys"> </Link>
                    </div>
                </h5>
                <form className="survey" onSubmit={handleSubmit}>
                    <h6>{survey.title}</h6>
                    <hr/>
                    {!survey.questions.length ?
                        <p className="primary text-center fw-bold size-12">{intl.formatMessage({id: 'survey.empty_questions'})}</p> :
                        <>
                            <Questions questions={survey.questions} isChangeableSurvey={isChangeableSurvey} />
                            {isChangeableSurvey &&
                                <button className="button m-t" type="submit">
                                    <FormattedMessage id="save"/>
                                </button>
                            }
                        </>
                    }
                </form>
            </div>
        </div>
    );
};

SurveyForm.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}),
    survey,
};

SurveyForm.defaultProps = {
    survey: surveyDefault
};

export default injectIntl(SurveyForm);
