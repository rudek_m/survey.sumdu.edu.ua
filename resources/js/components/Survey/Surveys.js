import React, {useContext, useEffect, useState} from 'react';
import {injectIntl, IntlProvider} from "react-intl";
import PropTypes from "prop-types";
import {withRouter} from 'react-router-dom';
import {CSSTransition} from "react-transition-group";

import useDataApi from "../../utils/hooks/useDataApi";
import ThreeCircleLoader from "../Loader/ThreeCircleLoader";
import userContext from "../../contexts/userContext";
//import StickyMessageSurveyContext from "../../contexts/stickyMessageSurveyContext";
import SurveysTable from "./SurveysTable/SurveysTable";
import SurveyMainSettingsForm from "./SurveyEdit/SurveyMainSettings/SurveyMainSettingsForm";

import {surveyDefault} from '../../types/survey';

const initialEditReveal = {
    open: false,
    data: surveyDefault
};

const Surveys = ({ intl, location }) => {
    //const { setStickySurveyState } = useContext(StickyMessageSurveyContext);

    const { rawData: surveysRawData, isLoading: isSurveyLoading, isError: isSurveyError } = useDataApi({
        url: '/surveys'
    });
    const surveys = surveysRawData && !isSurveyError ? typeof surveysRawData === 'object' ? Object.values(surveysRawData) : surveysRawData  : [];

    const { userState: {isAdmin}  } = useContext(userContext);


    useEffect(() => {
        if (typeof location.state !== 'undefined') {
            //setStickySurveyState(location.state.setStickySurveyState);
            history.replaceState(undefined, '');
        }
    });

    const [ editRevealState, setEditRevealState ] = useState({...initialEditReveal});

    const openEditReveal = () => {
        setEditRevealState({open: true, data: surveyDefault});
    };

    const closeEditReveal = () => {
        setEditRevealState(initialEditReveal);
    };

    return (
        <>
            {isSurveyLoading && <ThreeCircleLoader />}
            <CSSTransition in={!isSurveyLoading} timeout={300} appear={true} exit={false} classNames="tab">
                <div className={`clearfix ${isSurveyLoading ? 'tab-exit-done' : ''}`}>
                    <div className="small-12 column">
                        <div className="callout clearfix paddings">
                            <SurveysTable
                                surveys={surveys}
                                isAdmin={isAdmin}
                            />
                            {isAdmin ?
                                <>
                                    <SurveyMainSettingsForm
                                        {...editRevealState.data}
                                        open={editRevealState.open}
                                        closeEditReveal={closeEditReveal}
                                    />
                                    <button
                                        className="plus-button big add-question primary pulse"
                                        type="button"
                                        title={intl.formatMessage({id:'survey.add'})}
                                        onClick={openEditReveal}
                                    />
                                </> :
                                ''
                            }
                        </div>
                    </div>
                </div>
            </CSSTransition>
        </>
    )
};

Surveys.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}).isRequired
};

export default injectIntl(withRouter(Surveys));
