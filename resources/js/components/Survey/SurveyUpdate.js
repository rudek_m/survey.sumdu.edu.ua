import React from 'react';
import { useParams } from 'react-router-dom';

import useDataApi from "../../utils/hooks/useDataApi";
import ThreeCircleLoader from "../Loader/ThreeCircleLoader";
import SurveyEdit from "./SurveyEdit/SurveyEdit";
import {CSSTransition} from "react-transition-group";
import {surveyDefault} from "../../types/survey";

const SurveyUpdate = () => {
    const { id } = useParams();

    const { rawData: surveyRawData, isLoading: isSurveyLoading, isError: isSurveyError } = useDataApi({
        url: `/surveys/${id}`,
        params: {data: 'edit'}
    });

    const survey = surveyRawData && !isSurveyError ? surveyRawData : surveyDefault;

    return <>
        { isSurveyLoading && <ThreeCircleLoader /> }
        <CSSTransition in={!isSurveyLoading} timeout={300} appear={true} exit={false} classNames="tab">
            <div className={`tab ${isSurveyLoading ? 'tab-exit-done' : ''}`}>
                <SurveyEdit survey={survey} />
            </div>
        </CSSTransition>
    </>;
};

export default SurveyUpdate;
