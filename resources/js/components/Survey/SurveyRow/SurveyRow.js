import React from 'react';
import { Link } from 'react-router-dom';
import {FormattedDate, FormattedMessage} from 'react-intl';
import PropTypes from "prop-types";
import {injectIntl, IntlProvider} from "react-intl";


import { survey } from "../../../types/survey";

const SurveyRow = ({intl, survey, index, isAdmin, removeSurveyClick, copySurveyClick}) => {

    const removeSurvey = e => {
        e.preventDefault();
        removeSurveyClick({data: survey});
    };

    const copySurvey = e => {
        e.preventDefault();
        copySurveyClick(survey.id);
    };

    const from = new Date(survey.from);
    const to = new Date(survey.to);

    let statusClassName = survey.status === 1 ? {} : {className: 'success'};
    let status = survey.status_name;

    if ( survey.answers_exist ) {
        statusClassName = {};
        status = intl.formatMessage({id: "survey.status.3"});
    }
    if ( to < new Date().setHours(0,0,0, 0) ) {
        statusClassName = {};
        status = intl.formatMessage({id: "survey.status.4"})
    }

    return (
        <tr id={`survey-${survey.id}`}>
            <td className="text-center">{index+1}</td>
            <td>
                <Link to={`/surveys/${survey.id}`} className="underline">{survey.title}</Link>
                <i className="date-status column small-12 m-t-05 size-08 none w-space-normal">{`${intl.formatDate(from)} - ${intl.formatDate(to)}`}, <span {...statusClassName}>{status}</span></i>
            </td>
            <td className="text-center">
                <FormattedDate value={from}/> - <FormattedDate value={to}/></td>
            <td className={`text-center${statusClassName?.className ? ' ' + statusClassName.className : ''}`}>{status}</td>
            {isAdmin ?
                survey.is_accessible ? <td className="text-center">
                    <ul className="dropdown menu" data-dropdown-menu="true" data-disable-hover="true" data-click-open="true"
                        data-force-follow="false" data-alignment="left"
                        style={{verticalAlign: 'text-bottom', maxWidth: '40px', float:'right'}}>
                        <li>
                            <a className="font-awesome size-21" onClick={(e)=>{e.preventDefault()}} >&#xf0c9;</a>
                            <ul className="menu cabinet text-left" style={{minWidth:'165px'}}>
                                <li>
                                    <Link to={`/surveys/edit/${survey.id}`} className="tab-switcher">
                                        <span className="font-awesome va-bottom">&#xf040;</span> <FormattedMessage id="edit"/>
                                    </Link>
                                </li>
                                <li>
                                    <a className="tab-switcher" href="#" onClick={copySurvey}>
                                        <span className="font-awesome va-bottom">&#xf0c5;</span> <FormattedMessage id="copy"/>
                                    </a>
                                </li>
                                <li>
                                    <a className="tab-switcher" href="#" onClick={removeSurvey}>
                                        <span className="font-awesome va-bottom size-18">&#xf014;</span> <FormattedMessage id="remove"/>
                                    </a>
                                </li>
                                <li>
                                    <Link to={`/surveys/result/${survey.id}`} className="tab-switcher">
                                        <span className="font-awesome va-bottom">&#xf200;</span> <FormattedMessage id="result.result"/>
                                    </Link>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </td> : <td> </td>
                : null
            }
        </tr>
    );
};

SurveyRow.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}),
    survey: survey.isRequired,
    index: PropTypes.number.isRequired,
    isAdmin: PropTypes.bool.isRequired,
    removeSurveyClick: PropTypes.func,
    copySurveyClick: PropTypes.func
};


export default injectIntl(SurveyRow);


