import React from 'react';
import PropTypes from "prop-types";
import {TransitionGroup, CSSTransition} from "react-transition-group";

import { contingent } from "../../../../types/contingent";
import ContingentSettings from "./ContingentSettings";

const SurveyContingentsSettings = ({contingents, openEditReveal, openRemoveReveal }) => {
    return (
            <TransitionGroup>
                {contingents.map((contingent, index) => (
                    <CSSTransition key={contingent.id} classNames="item" timeout={500}>
                        <ContingentSettings
                            key={index} index={index}
                            contingent={contingent}
                            countContingents={contingents.length}
                            openEditReveal={openEditReveal}
                            openRemoveReveal={openRemoveReveal}
                        />
                    </CSSTransition>
                ))}
            </TransitionGroup>
    )
};

SurveyContingentsSettings.propTypes = {
    contingents: PropTypes.arrayOf(contingent),
    openEditReveal: PropTypes.func.isRequired,
    openRemoveReveal: PropTypes.func.isRequired
};

export default SurveyContingentsSettings;
