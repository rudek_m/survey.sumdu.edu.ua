import React, { Component } from "react";
import PropTypes from "prop-types";
import {FormattedMessage, injectIntl, IntlProvider} from "react-intl";
import { CSSTransition } from "react-transition-group";

import RevealForm from "../../../Reveal/RevealForm/RevealForm";
import Autocomplete from "../../../Autocomplete/Autocomplete";
import CheckboxRadio from "../../../CheckboxRadio/CheckboxRadio";
import fetchData from "../../../../utils/helper/fetchData";
import ErrorBoundaryContext from "../../../../contexts/errorBoundaryContext";
import { popupNotify } from "../../../../utils/helper/popupNotify";
import { contingent } from "../../../../types/contingent";

class SurveyContingentSettingsForm extends Component {
    constructor(props) {
        super(props);
        this.reveal = React.createRef();
        this.notify = React.createRef();
        this.state = {
            alert: '',
            primary: '',
            notify: false,
            open: props.editContingentState.open,
            ...props.editContingentState.data,
            ...{id_survey: props.idSurvey}
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.changeAutocomplete = this.changeAutocomplete.bind(this);
        this.changeCategory = this.changeCategory.bind(this);
        this.changeCheckbox = this.changeCheckbox.bind(this);
    }

    static getDerivedStateFromProps(props, state) {
        if (props.editContingentState.open !== state.open) {
            return {
                alert: '',
                primary: '',
                notify: false,
                open: props.editContingentState.open,
                ...props.editContingentState.data,
                id_survey: props.idSurvey
            };
        }
        return null;
    }

    async handleSubmit (e) {
        e.preventDefault();
        try {
            const formData = new FormData(e.target);
            formData.append('id_survey', this.state.id_survey);

            ['id_course', 'id_degree', 'id_group',  'id_specialty', 'name_specialty', 'name_degree', 'name_course', 'name_group',].forEach(
                field => {
                    if ( (this.state.id_categ1 & 14) === 0 )  {
                        formData.append(field, '');
                    }
                }
            );

            let url = '/contingents';
            if ( this.state.id ) {
                url += `/${this.state.id}?_method=PUT`;
            }
            this.props.setBigCircleLoaderState(true);
            const { data: { data } } = await fetchData({ url, method: 'POST', data: formData });

            this.props.setSurveyConfig(prevState => ({
                ...prevState,
                ...{
                    contingents: !this.state.id ?
                        [...prevState.contingents, data] :
                        prevState.contingents.map(contingent => contingent.id === data.id ? data : contingent)
                }
            }));
            this.props.closeEditReveal();
        } catch (error) {
            const errorState = {type: 'alert', message: this.props.intl.formatMessage({id: 'errors.ajax'})};
            if (error.response) {
                if ( [401, 404].includes(error.response.status) ) {
                    this.context.setErrorState({hasError: true, error: error.response});
                }
                errorState.message = error.response.data.message;
            }
            popupNotify(this.setState.bind(this), 'alert', errorState.message);
        } finally {
            this.props.setBigCircleLoaderState(false);
        }
    }

    changeAutocomplete(data) {
        this.setState(() => data);
    }

    changeCategory({target: {name, value, checked }}) {
        name = name.substr(0,6);
        this.setState(state => (
            {[`id_${name}`]: checked ? state[`id_${name}`] + (+value) : state[`id_${name}`] - (+value)}
        ));
    }

    changeCheckbox({target: {name, checked}}) {
        this.setState(() => (
            { [name]: checked ? 1 : 0 }
        ));
    }

    render() {
        return (
            <RevealForm idReveal="contingent" onSubmit={this.handleSubmit} open={this.state.open} revealRef={this.reveal} classes={''}>
                <h5 className={`callout cabinet small ${this.state.notify ? 'margin-left-right' : 'margin-left-bottom-right'}`}>
                    {this.props.intl.formatMessage({id: 'contingent'})}
                    <button className="close-button" onClick={this.props.closeEditReveal} aria-label="Close reveal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </h5>
                <CSSTransition in={this.state.notify} timeout={200} classNames="slide">
                    <div className="slide">
                        <div className="small-12 column text-center min-height-info mb-8px notify" ref={this.notify}>
                            <b className="alert">{this.state.alert}</b>
                            <b className="primary">{this.state.primary}</b>
                        </div>
                    </div>
                </CSSTransition>
                <div className="small-12 column">
                    <label>{<FormattedMessage id="contingent.category"/>}</label>
                    <table className="checkbox-radio stack-xxsmall b-none bg-none m-b-n">
                        <colgroup>
                            <col width="150px"/>
                            <col width="150px"/>
                            <col width="150px"/>
                        </colgroup>
                        <tbody>
                            <tr>
                                <td>
                                    <CheckboxRadio
                                        id="students"
                                        type="checkbox"
                                        name="categ1[]"
                                        value="2"
                                        label="contingent.students.2"
                                        checked={!!(this.state.id_categ1 & 2)}
                                        changeHandler={this.changeCategory}
                                    />
                                </td>
                                <td>
                                    <CheckboxRadio
                                        id="postgraduates"
                                        type="checkbox"
                                        name="categ1[]"
                                        value="4"
                                        label="contingent.students.4"
                                        checked={!!(this.state.id_categ1 & 4)}
                                        changeHandler={this.changeCategory}
                                    />
                                </td>
                                <td>
                                    <CheckboxRadio
                                        id="graduates"
                                        type="checkbox"
                                        name="categ1[]"
                                        value="8"
                                        label="contingent.students.8"
                                        checked={!!(this.state.id_categ1 & 8)}
                                        changeHandler={this.changeCategory}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <CheckboxRadio
                                        id="employees"
                                        type="checkbox"
                                        name="categ2[]"
                                        value="2"
                                        label="contingent.employees.2"
                                        checked={!!(this.state.id_categ2 & 2)}
                                        changeHandler={this.changeCategory}
                                    />
                                </td>
                                <td>
                                    <CheckboxRadio
                                        id="teachers"
                                        type="checkbox"
                                        name="categ2[]"
                                        value="4"
                                        label="contingent.employees.4"
                                        checked={!!(this.state.id_categ2 & 4)}
                                        changeHandler={this.changeCategory}
                                    />
                                </td>
                                <td>
                                    <CheckboxRadio
                                        id="managers"
                                        type="checkbox"
                                        name="categ2[]"
                                        value="8"
                                        label="contingent.employees.8"
                                        checked={!!(this.state.id_categ2 & 8)}
                                        changeHandler={this.changeCategory}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className="small-12 column">
                    <Autocomplete
                        idPrefix="department"
                        label="contingent.employees.department"
                        url="/cabinet/autocomplete/departments"
                        name="div"
                        idValue={this.state.id_div}
                        nameValue={this.state.name_div}
                        changeAutocomplete={this.changeAutocomplete}
                        disabled={!this.state.id_div && !(this.state.id_categ1 & 14) && !(this.state.id_categ2 & 14)}
                    />
                </div>
                <div className="small-12 column m-b-small">
                    <CheckboxRadio
                        id="not-include-subdep"
                        type="checkbox"
                        name="not_include_subdep"
                        value="1"
                        label="contingent.not_include_subdep"
                        checked={!!this.state.not_include_subdep}
                        changeHandler={this.changeCheckbox}
                        disabled={!this.state.not_include_subdep && !this.state.id_div}
                    />
                </div>
                <div className="small-12 column m-b-small">
                    <CheckboxRadio
                        id="main-post"
                        type="checkbox"
                        name="main_post"
                        value="1"
                        label="contingent.main_post"
                        checked={!!this.state.main_post}
                        changeHandler={this.changeCheckbox}
                        disabled={!this.state.main_post && !(this.state.id_categ2 & 14)}
                    />
                </div>
                <div className="small-12 column">
                    <Autocomplete
                        idPrefix="specialty"
                        label="contingent.students.specialty"
                        url="/cabinet/autocomplete/specialties"
                        name="specialty"
                        idValue={this.state.id_specialty}
                        nameValue={this.state.name_specialty}
                        changeAutocomplete={this.changeAutocomplete}
                        disabled={!this.state.id_specialty && !(this.state.id_categ1 & 14)}
                    />
                </div>
                <div className="small-12 column">
                    <Autocomplete
                        idPrefix="degree"
                        label="contingent.students.degree"
                        url="/cabinet/autocomplete/degrees"
                        name="degree"
                        idValue={this.state.id_degree}
                        nameValue={this.state.name_degree}
                        changeAutocomplete={this.changeAutocomplete}
                        disabled={!this.state.id_degree && !(this.state.id_categ1 & 14)}
                    />
                </div>
                <div className="small-12 xxxlsmall-6 column">
                    <Autocomplete
                        idPrefix="form"
                        label="contingent.students.form"
                        url="/cabinet/autocomplete/form"
                        name="form"
                        idValue={this.state.id_form}
                        nameValue={this.state.name_form}
                        changeAutocomplete={this.changeAutocomplete}
                        disabled={!this.state.id_form && !(this.state.id_categ1 & 14)}
                    />
                </div>
                <div className="small-12 xxxlsmall-6 column">
                    <Autocomplete
                        idPrefix="kod_std"
                        label="contingent.students.kod_std"
                        url="/cabinet/autocomplete/kod_std"
                        name="kod_std"
                        idValue={this.state.id_kod_std}
                        nameValue={this.state.name_kod_std}
                        changeAutocomplete={this.changeAutocomplete}
                        disabled={!this.state.id_kod_std && !(this.state.id_categ1 & 14)}
                    />
                </div>
                <div className="small-12 xxxlsmall-6 column">
                    <Autocomplete
                        idPrefix="course"
                        label="contingent.students.course"
                        url="/cabinet/autocomplete/courses"
                        name="course"
                        idValue={this.state.id_course}
                        nameValue={this.state.name_course}
                        changeAutocomplete={this.changeAutocomplete}
                        disabled={!this.state.id_course && !(this.state.id_categ1 & 14)}
                    />
                </div>
                <div className="small-12 xxxlsmall-6 column">
                    <Autocomplete
                        inputClass="margin-none"
                        idPrefix="group"
                        label="contingent.students.group"
                        url="/cabinet/autocomplete/groups"
                        name="group"
                        idValue={this.state.id_group}
                        nameValue={this.state.name_group}
                        changeAutocomplete={this.changeAutocomplete}
                        disabled={!this.state.id_group && !(this.state.id_categ1 & 14)}
                    />
                </div>
                <div className="small-12 column clearfix float-left">
                    <input type="submit" className="button m-t" value={this.props.intl.formatMessage({id: 'save'})} title={this.props.intl.formatMessage({id: 'save'})}/>{" "}
                    <input type="button" className="button hollow close m-t" onClick={this.props.closeEditReveal} value={this.props.intl.formatMessage({id: 'cancel'})} title={this.props.intl.formatMessage({id: 'cancel'})}/>
                </div>
            </RevealForm>
        )
    }
}

SurveyContingentSettingsForm.contextType = ErrorBoundaryContext;

SurveyContingentSettingsForm.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}),
    idSurvey: PropTypes.number.isRequired,
    editContingentState: PropTypes.shape({
        open: PropTypes.bool.isRequired,
        data: contingent
    }),
    setSurveyConfig: PropTypes.func.isRequired,
    closeEditReveal: PropTypes.func.isRequired,
    setBigCircleLoaderState: PropTypes.func.isRequired,
};

export default injectIntl(SurveyContingentSettingsForm);
