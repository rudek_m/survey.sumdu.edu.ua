import React from 'react';
import PropTypes from 'prop-types';
import {IntlProvider, injectIntl, FormattedMessage} from "react-intl";

import { contingent } from '../../../../types/contingent';

const ContingentSettings = ({intl, index, countContingents, contingent, openEditReveal, openRemoveReveal }) => {
    const removeContingentClick = () => {
        openRemoveReveal({data: {text: `${intl.formatMessage({id: 'contingent'})} ${index+1}`, id: contingent.id }, type:'contingent'});
    };

    const editContingentClick = () => {
        openEditReveal(contingent);
    };

    const categoriesMasks = (masks, category) =>
        [2,4,8].filter( mask => ((masks & mask) > 0) ).map(
            (mask, index) => (<React.Fragment key={index}>{index > 0 ? ', ' : ''}<FormattedMessage id={`contingent.${category}.${mask}`} /></React.Fragment>)
        );

    return (
        <div className={`callout question-item ${countContingents === index + 1 ? 'margin-left-right' : 'margin-left-bottom-right'} padding-top clearfix`}>
            <div className="legend">{intl.formatMessage({id: 'contingent'})} {index+1}</div>
            <button className="small edit-button primary font-awesome edit" type="button" onClick={editContingentClick} style={{right: "25px"}}>&#xf040;</button>
            <button className="small edit-button primary font-awesome remove" type="button" onClick={removeContingentClick}>&#xf014;</button>

            <div className="small-12 column m-b">
                <p className="margin-bottom-half l-h-1-4">
                    <b><FormattedMessage id="contingent.category"/></b><br/>
                    <span className="lowercase">{categoriesMasks(contingent.id_categ1, 'students')}{(!!contingent.id_categ1 && !!contingent.id_categ2) && ', '}{categoriesMasks(contingent.id_categ2, 'employees')} {(!!contingent.main_post && <b>(<FormattedMessage id="contingent.main_post"/>)</b>)}</span>
                </p>
                {contingent.name_specialty &&
                    <p className="margin-bottom-half l-h-1-4">
                        <b><FormattedMessage id="contingent.students.specialty"/></b><br/>
                        <span>{contingent.name_specialty}</span>
                    </p>
                }
                {contingent.name_degree &&
                    <p className="margin-bottom-half l-h-1-4">
                        <b><FormattedMessage id="contingent.students.degree"/></b><br/>
                        {contingent.name_degree}
                    </p>
                }
                {contingent.name_course &&
                    <p className="margin-bottom-half l-h-1-4">
                        <b><FormattedMessage id="contingent.students.course"/></b> {contingent.name_course}
                    </p>
                }
                {contingent.name_group &&
                    <p className="margin-bottom-half l-h-1-4">
                        <b><FormattedMessage id="contingent.students.group"/></b> {contingent.name_group}
                    </p>
                }
                {contingent.name_form &&
                    <p className="margin-bottom-half l-h-1-4">
                        <b><FormattedMessage id="contingent.students.form"/></b> {contingent.name_form}
                    </p>
                }
                {contingent.name_kod_std &&
                    <p className="margin-bottom-half l-h-1-4">
                        <b><FormattedMessage id="contingent.students.kod_std"/></b> {contingent.name_kod_std}
                    </p>
                }
                {contingent.name_div &&
                    <p className="margin-bottom-half l-h-1-4">
                        <b><FormattedMessage id="contingent.employees.department"/></b><br/>
                        <span className="lowercase">{contingent.name_div} {(!!contingent.not_include_subdep && <b>(<FormattedMessage id="contingent.not_include_subdep"/>)</b>)}</span>
                    </p>
                }
            </div>
        </div>
    )
};


ContingentSettings.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}),
    contingent,
    index: PropTypes.number.isRequired,
    countContingents: PropTypes.number.isRequired,
    openEditReveal: PropTypes.func.isRequired,
    openRemoveReveal: PropTypes.func.isRequired
};

export default injectIntl(ContingentSettings);
