import React, {useContext, useEffect, useState} from "react";
import {FormattedMessage, injectIntl, IntlProvider} from "react-intl";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import {CSSTransition, TransitionGroup} from "react-transition-group";

import SurveyMainSettings from "./SurveyMainSettings/SurveyMainSettings";
import SurveyContingentSettings from "./SurveyContingentSettings/SurveyContingentsSettings";
import SurveyQuestionSettings from "./SurveyQuestionSettings/SurveyQuestionsSettings"
import fetchData from "../../../utils/helper/fetchData";
import StickyMessageSurveyContext from "../../../contexts/stickyMessageSurveyContext";
import errorBoundaryContext from "../../../contexts/errorBoundaryContext";
import SurveyQuestionSettingsForm from "./SurveyQuestionSettings/SurveyQuestionSettingsForm";
import RevealRemove from "../../Reveal/RevealRemove/RevealRemove";
import { questionDefault } from "../../../types/question";
import { contingentDefault} from "../../../types/contingent";
import BigCircleLoaderContext from "../../../contexts/bigCircleLoaderContext";
import SurveyContingentSettingsForm from "./SurveyContingentSettings/SurveyContingentSettingsForm";
import {surveyDefault} from "../../../types/survey";

const initialRemoveReveal = {
    open: false,
    type: '',
    data: {
        text: ''
    }
};

const initialQuestionEditReveal = {
    open: false,
    data: questionDefault
};

const initialContingentEditReveal = {
    open: false,
    data: contingentDefault
};

const SurveyEdit = ({survey, intl}) => {
    const { setErrorState } = useContext(errorBoundaryContext);
    const { setStickySurveyState } = useContext(StickyMessageSurveyContext);
    const { setBigCircleLoaderState } = useContext(BigCircleLoaderContext);

    const [ surveyConfig, setSurveyConfig ] = useState(survey);
    const [ removeRevealState, setRemoveRevealState ] = useState(initialRemoveReveal);
    const [ editQuestionRevealState, setEditQuestionRevealState ] = useState(initialQuestionEditReveal);
    const [ editContingentRevealState, setEditContingentRevealState ] = useState(initialContingentEditReveal);
    const [ tabState, setTabState ] = useState(1);


    useEffect(() => {
        setSurveyConfig(survey);
    },[survey.id]);

    const openRemoveReveal = ({data, type}) => {
        setRemoveRevealState( {open: true, type, data });
    };

    const closeRemoveReveal = () => {
        setRemoveRevealState(initialRemoveReveal);
    };

    const openEditQuestionReveal = (data) => {
        setEditQuestionRevealState( {open: true, data});
    };

    const closeEditQuestionReveal = () => {
        setEditQuestionRevealState(initialQuestionEditReveal);
    };

    const openEditContingentReveal = (data) => {
        setEditContingentRevealState( {open: true, data});
    };

    const closeEditContingentReveal = () => {
        setEditContingentRevealState(initialContingentEditReveal);
    };

    const addClick = () => {
        switch(tabState) {
            case 1:
                openEditContingentReveal(initialContingentEditReveal.data);
            break;
            case 2:
                openEditQuestionReveal(initialQuestionEditReveal.data);
            break;
        }
    };

    const toggleTab = (e) => {
        setTabState(+e.target.dataset.tab);
    };

    const removeHandler = async (e) => {
        e.preventDefault();
        try {
            const url = `/${removeRevealState.type}s/${removeRevealState.data.id}?_method=DELETE`;
            const { data: {message, status, data} } = await fetchData({ url, method:'POST'});
            if (status === 'success' && data === true) {
                setSurveyConfig({
                    ...surveyConfig,
                    ...{[`${removeRevealState.type}s`]: surveyConfig[`${removeRevealState.type}s`].filter(item => item.id !== removeRevealState.data.id)}
                });
                setStickySurveyState({type:'primary', message});
            } else {
                setStickySurveyState({type:'alert', message: intl.formatMessage({id: 'survey.error.remove'})});
            }
        } catch (error) {
            const errorState = {type: 'alert', message: intl.formatMessage({id: 'errors.ajax'})};
            if (error.response) {
                if ([401, 404].includes(error.response.status)) {
                    setErrorState({hasError: true, error: error.response});
                } else {
                    if (error.response) {
                        errorState.message = error.response.data.message;
                    }
                }
            }
            setStickySurveyState(errorState);
        } finally {
            closeRemoveReveal();
        }
    };

    return (
        <div className="small-12 column">
            <div className="callout clearfix padding-top-bottom">
                <h5 className="callout cabinet small margin-left-bottom-right clearfix one-icon" style={{marginBottom: '32px'}}>
                    <FormattedMessage id={'survey.settings'} />
                    <div className="h-icons">
                        <Link to="/" className="surveys" title={intl.formatMessage({id: 'survey.title'})}/>
                    </div>
                </h5>
                <div className="survey settings-survey">
                    <SurveyMainSettings survey={surveyConfig} setSurveyConfig={setSurveyConfig}/>
                    {!!surveyConfig.id && (
                        <div className="custom-tabs clearfix">
                            <div className="button-group no-gaps text-center" style={{marginBottom: '34px'}}>
                                <a className={`button ${tabState === 1 ? 'primary' : 'hollow'}`} data-tab={1} onClick={toggleTab}>
                                    <FormattedMessage id={'contingent'}/>
                                </a>
                                <a className={`button ${tabState === 2 ? 'primary' : 'hollow'}`} data-tab={2} onClick={toggleTab}>
                                    <FormattedMessage id={'question'}/>
                                </a>
                            </div>
                            <CSSTransition in={tabState === 1} timeout={300} classNames="tab" exit={false}>
                                <div className={`tab`}>
                                    {!!surveyConfig.contingents.length ? <>
                                        <SurveyContingentSettings
                                            contingents={surveyConfig.contingents}
                                            openEditReveal={openEditContingentReveal}
                                            openRemoveReveal={openRemoveReveal}
                                        />
                                    </> : <p className="primary text-center fw-bold size-12 margin-bottom-none" style={{'marginTop':'-16px'}}>
                                        <FormattedMessage id={'contingent.empty'}/>
                                    </p>}
                                </div>
                            </CSSTransition>
                            <CSSTransition in={tabState === 2} timeout={300} classNames="tab" exit={false}>
                                <div className={`tab ${tabState === 1 ? 'tab-exit-done' : ''}`}>
                                    {!!surveyConfig.questions.length ? <>
                                        <SurveyQuestionSettings
                                            questions={surveyConfig.questions}
                                            openEditReveal={openEditQuestionReveal}
                                            openRemoveReveal={openRemoveReveal}
                                        />
                                    </> : <p className="primary text-center fw-bold size-12 margin-bottom-none" style={{'marginTop':'-16px'}}>
                                        <FormattedMessage id={'survey.empty_questions'}/>
                                    </p>}
                                </div>
                            </CSSTransition>
                            <SurveyContingentSettingsForm
                                idSurvey={surveyConfig.id}
                                setSurveyConfig={setSurveyConfig}
                                editContingentState={editContingentRevealState}
                                closeEditReveal={closeEditContingentReveal}
                                setBigCircleLoaderState={setBigCircleLoaderState}
                            />
                            <SurveyQuestionSettingsForm
                                idSurvey={surveyConfig.id}
                                open={editQuestionRevealState.open}
                                editQuestionState={editQuestionRevealState.data}
                                setSurveyConfig={setSurveyConfig}
                                closeEditReveal={closeEditQuestionReveal}
                                setBigCircleLoaderState={setBigCircleLoaderState}
                            />
                        </div>
                    )}
                </div>
            </div>
            <button
                className="plus-button big add-question primary pulse"
                type="button"
                onClick={addClick}
                title={intl.formatMessage({id: ['contingent', 'question'][tabState-1]+'.add'})}
            />
            <RevealRemove
                id="question-contingent-remove"
                open={removeRevealState.open}
                type={removeRevealState.type}
                title={removeRevealState.data.text}
                onClose={closeRemoveReveal}
                onRemove={removeHandler}
            />
        </div>
    );
};

SurveyEdit.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}).isRequired,
    survey: PropTypes.object
};

SurveyEdit.defaultProps = {
    survey: surveyDefault
};


export default injectIntl(SurveyEdit);
