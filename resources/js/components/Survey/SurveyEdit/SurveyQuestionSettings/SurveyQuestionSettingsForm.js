import React, { Component } from "react";
import PropTypes from "prop-types";
import { injectIntl, IntlProvider, FormattedMessage } from "react-intl";
import {CSSTransition, TransitionGroup} from "react-transition-group";

import RevealForm from "../../../Reveal/RevealForm/RevealForm";
import ErrorBoundaryContext from "../../../../contexts/errorBoundaryContext";
import fetchData from "../../../../utils/helper/fetchData";
import { popupNotify } from "../../../../utils/helper/popupNotify";
import CheckboxRadio from "../../../CheckboxRadio/CheckboxRadio";

class SurveyQuestionSettingsForm extends Component {
    constructor(props) {
        super(props);
        this.reveal = React.createRef();
        this.notify = React.createRef();
        this.state = {
            alert: '',
            primary: '',
            notify: false,
            open: props.open,
            text: props.editQuestionState.text,
            typeQuestion: props.editQuestionState.type,
            variants: props.editQuestionState.variants
        };
        this.typesQuestion = {
            'single': 'radio',
            'multi': 'checkbox',
            'text': 'radio'
        };
        this.maxCountVariants = 20;
        this.changeText = this.changeText.bind(this);
        this.changeType = this.changeType.bind(this);
        this.addVariant = this.addVariant.bind(this);
        this.changeVariant = this.changeVariant.bind(this);
        this.removeVariant = this.removeVariant.bind(this);
        this.closeReveal = this.closeReveal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSave = this.handleSave.bind(this);
    }

    static getDerivedStateFromProps(props, state) {
        if (props.open !== state.open) {
            return {
                alert: '',
                primary: '',
                notify: false,
                open: props.open,
                text: props.editQuestionState.text,
                typeQuestion: props.editQuestionState.type,
                variants: props.editQuestionState.variants
            };
        }
        return null;
    }

    async handleSave(e) {
        e.preventDefault();
        const before = () => {

            const formData = new FormData(e.target);
            formData.append('question[id_survey]', this.props.idSurvey.toString());
            let url = '/questions';
            if ( this.props.editQuestionState.id ) {
                url += `/${this.props.editQuestionState.id}?_method=PUT`
            }

            return {url, data: formData, method: 'POST'}
        };
        const success = ({data: {data, status}}) => {
            if (status === 'success') {
                this.props.setSurveyConfig( prevState => ({
                        ...prevState,
                        ...{
                            questions: !this.props.editQuestionState.id ?
                                [...prevState.questions, data ] :
                                prevState.questions.map( question => {
                                    if (question.id === data.id) {
                                        return data;
                                    }
                                    return question;
                                })
                        }
                    }
                ));
                this.props.closeEditReveal();
            }
        };
        this.handleSubmit({before, success});
    }

    async handleSubmit({ before, success }) {
        try {
            this.props.setBigCircleLoaderState(true);
            success(await fetchData(before()));
        } catch(e) {
            const errorState = {type: 'alert', message: this.props.intl.formatMessage({id: 'errors.ajax'})};
            if (e.response) {
                if ( [401, 404].includes(e.response.status) ) {
                    this.context.setErrorState({hasError: true, error: e.response});
                }
                errorState.message = e.response.data.message;
            }
            popupNotify(this.setState.bind(this), 'alert', errorState.message);
        } finally {
            this.props.setBigCircleLoaderState(false);
        }
    }

    changeText(event) {
        const value = event.target.value;
        this.setState(() => ({ text: value }));
    }

    changeType(event) {
        const value = event.target.value;

        this.setState(state => {
            let variants = [];
            if ( ['single', 'multi', 'text'].includes(value) && state.variants.length && state.variants[0].text ) {
                variants = state.variants;
            }

            return {
                typeQuestion: value,
                variants
            }
        });
    }

    addVariant(event) {
        if (event.type === 'keydown' && ( event.which || event.keyCode ) === 9) {
            return true; //tabulation not add new input for variant
        }
        if ( this.state.variants.length < this.maxCountVariants ) {
            this.setState(state => ({
                variants: [...state.variants, {text: '', id: '', id_ques: this.props.editQuestionState.id, key: -this.state.variants.length * Math.random()}]
            }));
        }
    }

    removeVariant(event) {
        const target = event.target;
        this.setState(state => ({
            variants: state.variants.filter((variant, index) => +target.dataset.index !== index )
            })
        );
    }

    changeVariant(event) {
        const target = event.target;
        this.setState(state => ({
            variants: state.variants.map(
                (variant, index) =>
                    +target.dataset.index === index ? {
                        ...variant, text: target.value, id_ques: this.props.editQuestionState.id
                    } :
                    variant
            )})
        );
    }

    closeReveal(event) {
        event.preventDefault();
        this.props.closeEditReveal();
    }

    render() {
        return (
            <RevealForm idReveal="question" onSubmit={this.handleSave} open={this.props.open} revealRef={this.reveal} classes={''}>
                <h5 className={`callout cabinet small ${this.state.notify ? ' margin-left-right' : 'margin-left-bottom-right'}`}>
                    <FormattedMessage id="question"/>
                    <button className="close-button" type="button" onClick={this.closeReveal} aria-label="Close reveal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </h5>
                <CSSTransition in={this.state.notify} timeout={200} classNames="slide">
                    <div className="slide">
                        <div className="small-12 column text-center min-height-info notify" ref={this.notify}>
                            <b className="alert">{this.state.alert}</b>
                            <b className="primary">{this.state.primary}</b>
                        </div>
                    </div>
                </CSSTransition>
                <div className="small-12 column">
                    <textarea name="question[text]" value={this.state.text} onChange={this.changeText}/>
                    <label><FormattedMessage id="question.type"/></label>
                    <div className="md-tab-switcher stack choose">
                        <CheckboxRadio
                            id="single"
                            name="question[type]"
                            type="radio"
                            value="single"
                            label="question.type.single"
                            checked={this.state.typeQuestion === 'single'}
                            changeHandler={this.changeType}
                        />
                        <CheckboxRadio
                            id="multiple"
                            name="question[type]"
                            type="radio"
                            value="multi"
                            label="question.type.multiple"
                            checked={this.state.typeQuestion === 'multi'}
                            changeHandler={this.changeType}
                        />
                        <CheckboxRadio
                            id="text"
                            name="question[type]"
                            type="radio"
                            value="text"
                            label="question.type.extended"
                            checked={this.state.typeQuestion === 'text'}
                            changeHandler={this.changeType}
                        />
                    </div>

                    {['single', 'multi'].includes(this.state.typeQuestion) ?
                        <>
                            <label>{this.props.intl.formatMessage({id: "question.variants"})}</label>
                            <TransitionGroup>
                                {this.state.variants.map((variant, index) => (
                                    <CSSTransition key={variant.id || variant.key} classNames="item" timeout={500}>
                                        <div className="variant-answers add-answer" key={index}>
                                            <span className={`md-${this.typesQuestion[this.state.typeQuestion]}`}/>
                                            <input className="md margin-none" type="text"
                                                name={"variants[]"}
                                                value={variant.text}
                                                placeholder={this.props.intl.formatMessage({id: "question.variant.answer"})}
                                                data-index={index}
                                                autoFocus={this.state.variants.length === (index + 1)}
                                                autoComplete="off"
                                                onChange={this.changeVariant}
                                            />
                                            <button className="cross-button small" type="button" data-index={index} onClick={this.removeVariant}/>
                                        </div>
                                    </CSSTransition>
                                ))}
                            </TransitionGroup>
                            {this.state.variants.length !== this.maxCountVariants &&
                                <div className="variant-answers add-answer">
                                    <span className={`md-${this.typesQuestion[this.state.typeQuestion]}`}/>
                                    <input className="md margin-none"
                                           type="text"
                                           placeholder={this.props.intl.formatMessage({id: "question.variant.answer"})}
                                           autoComplete="off"
                                           onClick={this.addVariant}
                                           onKeyDown={this.addVariant}
                                    />
                                </div>
                            }
                        </> :
                        <div className="textarea-wrapper">
                            <textarea className="margin-none" readOnly={true}/>
                        </div>
                    }
                </div>
                <div className="small-12 column clearfix float-left">
                    <button className="button m-t" title={this.props.intl.formatMessage({id: 'save'})}>
                        {this.props.intl.formatMessage({id: 'save'})}
                    </button>{" "}
                    <button className="button hollow close m-t" onClick={this.closeReveal} title={this.props.intl.formatMessage({id: 'cancel'})}>
                        {this.props.intl.formatMessage({id: 'cancel'})}
                    </button>
                </div>
            </RevealForm>
        )
    }
}
SurveyQuestionSettingsForm.contextType = ErrorBoundaryContext;

SurveyQuestionSettingsForm.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}),
    idSurvey: PropTypes.number.isRequired,
    open: PropTypes.bool.isRequired,
    editQuestionState: PropTypes.object.isRequired,
    setSurveyConfig: PropTypes.func.isRequired,
    closeEditReveal: PropTypes.func.isRequired,
    setBigCircleLoaderState: PropTypes.func.isRequired
};


export default injectIntl(SurveyQuestionSettingsForm);
