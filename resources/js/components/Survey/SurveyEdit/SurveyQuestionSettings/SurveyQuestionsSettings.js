import React from 'react';
import {injectIntl, IntlProvider} from "react-intl";
import PropTypes from "prop-types";
import {CSSTransition, TransitionGroup} from "react-transition-group";

import { question } from "../../../../types/question";
import QuestionSettings from "./QuestionSettings";

const SurveyQuestionsSettings = ({questions, openEditReveal, openRemoveReveal }) => {
    return (
            <TransitionGroup>
                {questions.map((question, index) => (
                    <CSSTransition key={question.id} classNames="item" timeout={500}>
                        <QuestionSettings
                            key={index} index={index}
                            question={question}
                            countQuestions={questions.length}
                            openEditReveal={openEditReveal}
                            openRemoveReveal={openRemoveReveal}
                        />
                    </CSSTransition>
                ))}
            </TransitionGroup>
    )
};

SurveyQuestionsSettings.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}),
    questions: PropTypes.arrayOf(question),
    openEditReveal: PropTypes.func.isRequired,
    openRemoveReveal: PropTypes.func.isRequired
};

export default injectIntl(SurveyQuestionsSettings);
