import React from 'react';
import PropTypes from 'prop-types';
import {IntlProvider, injectIntl} from "react-intl";

import { question } from '../../../../types/question'

const QuestionSettings = ({intl, index, countQuestions, question, openEditReveal, openRemoveReveal }) => {
    const removeQuestionClick = () => {
        openRemoveReveal({data: question, type:'question'});
    };

    const editQuestionClick = () => {
        openEditReveal(question);
    };

    return (
        <div className={`callout question-item ${countQuestions === index + 1 ? 'margin-left-right' : 'margin-left-bottom-right'} padding-top clearfix`}>
            <div className="legend">{intl.formatMessage({id: 'question'})} {index+1}</div>
            <button className="small edit-button primary font-awesome edit" type="button" onClick={editQuestionClick} style={{right: "25px"}}>&#xf040;</button>
            <button className="small edit-button primary font-awesome remove" type="button" onClick={removeQuestionClick}>&#xf014;</button>
            <div className="small-12 column m-b">
                <p className="fw-bold size-16 margin-bottom-half">
                    {question.text}
                </p>
                <ul className="answers">
                    {['single', 'multi'].includes(question.type) ?
                        question.variants.map((variant, id) => (
                            <li key={id}>
                                <label
                                    className={`md-${question.type === 'single' ? 'radio' : 'checkbox'}`}>{variant.text}</label>
                            </li>
                        )) :
                        <li className="textarea-wrapper">
                            <textarea className="margin-none" readOnly={true} />
                        </li>
                    }
                </ul>
            </div>
        </div>
    )
};


QuestionSettings.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}),
    question,
    index: PropTypes.number.isRequired,
    countQuestions: PropTypes.number.isRequired,
    openEditReveal: PropTypes.func.isRequired,
    openRemoveReveal: PropTypes.func.isRequired
};

export default injectIntl(QuestionSettings);
