import React, { useState } from 'react';
import { FormattedMessage, injectIntl, IntlProvider } from "react-intl";
import PropTypes from "prop-types";

import SurveyMainSettingsForm from "./SurveyMainSettingsForm";
import {survey, surveyDefault} from '../../../../types/survey';

const initialEditReveal = {
    open: false,
    data: surveyDefault
};

const SurveyMainSettings = ({ intl, survey, setSurveyConfig }) => {
    const [ editRevealState, setEditRevealState ] = useState({...initialEditReveal});

    const openEditReveal = () => {
        setEditRevealState({open: true, data: survey});
    };

    const closeEditReveal = () => {
        setEditRevealState(initialEditReveal);
    };

    return (
        <>
            <div className="callout margin-left-bottom-right theme padding-top clearfix">
                <div className="legend"><FormattedMessage id={`survey.settings.survey`}/></div>
                <button className="edit-button primary font-awesome" type="button" onClick={openEditReveal}>&#xf040;</button>

                <div className="small-12 column">
                    <h6 className=" m-b">{survey.title}</h6>
                    <p className="margin-bottom-half l-h-1-4">
                        <b>{intl.formatMessage({id: 'survey.settings.period'})}</b>{" "}
                        {intl.formatMessage({id: 'survey.settings.from'})}&nbsp;{intl.formatDate(survey.from)}&nbsp;{intl.formatMessage({id: 'survey.settings.to'})}&nbsp;{intl.formatDate(survey.to)}
                    </p>
                    <p>
                        <b><FormattedMessage id='survey.settings.status'/></b> {survey.status_name + (new Date(survey.to) < new Date().setHours(0,0,0, 0) ? `, ${intl.formatMessage({id: "survey.status.4"})}` : '')}
                    </p>
                </div>
            </div>
            <SurveyMainSettingsForm
                {...editRevealState.data}
                open={editRevealState.open}
                setSurveyConfig={setSurveyConfig}
                closeEditReveal={closeEditReveal}
            />
        </>
    );
};

SurveyMainSettings.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}).isRequired,
    survey,
    setSurveyConfig: PropTypes.func.isRequired
};

SurveyMainSettings.defaultProps = {
    survey: surveyDefault
};

export default injectIntl(SurveyMainSettings);
