import React, { Component } from 'react';
import PropTypes from "prop-types";
import {injectIntl, IntlProvider} from "react-intl";
import {withRouter} from "react-router-dom";
import {CSSTransition} from "react-transition-group";

import RevealForm from "../../../Reveal/RevealForm/RevealForm";
import DatePicker from "../../../DatePicker/DatePicker";
import fetchData from "../../../../utils/helper/fetchData";
import {popupNotify} from "../../../../utils/helper/popupNotify";
import ErrorBoundaryContext from "../../../../contexts/errorBoundaryContext";

class SurveyMainSettingsForm extends Component {

    constructor(props) {
        super(props);
        this.from = null;
        this.to = null;
        this.state = {
            alert: '',
            primary: '',
            notify: false,
            open: props.open,
            title: props.title,
            from: props.intl.formatDate(props.from),
            to: props.intl.formatDate(props.to),
            status: props.status
        };
        this.notify = React.createRef();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeReveal = this.closeReveal.bind(this);
        this.changeTitle = this.changeTitle.bind(this);
        this.changeDate = this.changeDate.bind(this);
        this.changeStatus = this.changeStatus.bind(this);
    }

    componentDidMount() {
        this.$from = $(this.from);
        this.$to = $(this.to);
    }

    componentWillUnmount() {
        this.$from.datepicker('hide');
        this.$from.datepicker('destroy');
        this.$to.datepicker('hide');
        this.$to.datepicker('destroy');
        $('#ui-datepicker-div').remove();
    }

    static getDerivedStateFromProps(props, state) {
        if (props.open !== state.open) {
            return {
                alert: '',
                primary: '',
                notify: false,
                open: props.open,
                title: props.title,
                from: props.intl.formatDate(props.from),
                to: props.intl.formatDate(props.to),
                status: props.status
            };
        }
        return null;
    }

    async handleSubmit (e) {
        e.preventDefault();
        try {
            const formData = new FormData(e.target);

            let url = '/surveys';
            if ( this.props.id ) {
                url += `/${this.props.id}?_method=PUT`;
            }
            const { data: { data, status } } = await fetchData({ url, method: 'POST', data: formData });
            if (status === 'success') {
                if (this.props.setSurveyConfig) {
                    this.props.setSurveyConfig(prevState => ({...prevState, ...data}));
                }
                this.props.closeEditReveal();
                if (!this.props.id) {
                    this.props.history.replace(`/surveys/edit/${data.id}`);
                }
            }
        } catch ( error ) {
            const errorState = {type: 'alert', message: this.props.intl.formatMessage({id: 'errors.ajax'})};
            if ( error.response ) {
                if ( [401, 404].includes(error.response.status) ) {
                    this.context.setErrorState({hasError: true, error: error.response});
                }
                errorState.message = error.response.data.message;
            }
            popupNotify(this.setState.bind(this), 'alert', errorState.message);
        }
    }

    closeReveal(event) {
        event.preventDefault();
        this.props.closeEditReveal();
    }

    changeTitle(event) {
        const title = event.target.value;
        this.setState(() => ({ title }));
    }

    changeDate(date) {
        this.setState(() => date);
    }

    changeStatus(event) {
        const status = event.target.value;
        this.setState(() => ({status}))
    }

    render() {
        return <RevealForm idReveal="survey" onSubmit={this.handleSubmit} open={this.props.open} classes={''}>
            <h5 className={`callout cabinet small ${this.state.notify ? ' margin-left-right' : 'margin-left-bottom-right'}`}>
                {this.props.intl.formatMessage({id: 'survey.title'})}
                <button className="close-button" onClick={this.props.closeEditReveal} aria-label="Close reveal" type="button" >
                    <span aria-hidden="true">&times;</span>
                </button>
            </h5>
            <CSSTransition in={this.state.notify} timeout={200} classNames="slide">
                <div className="slide">
                    <div className="small-12 column text-center min-height-info mb-8px notify" ref={this.notify}>
                        <b className="alert">{this.state.alert}</b>
                        <b className="primary">{this.state.primary}</b>
                    </div>
                </div>
            </CSSTransition>
            <div className="small-12 column">
                <label htmlFor="theme">{this.props.intl.formatMessage({id: 'survey.settings.theme'})}</label>
                <textarea id="theme" name="survey[title]" value={this.state.title} onChange={this.changeTitle}/>
            </div>
            <div className="small-12 column">
                <label>{this.props.intl.formatMessage({id: 'survey.settings.period'})}</label>
            </div>
            <DatePicker classDiv="from" name="survey[from]" datePlaceholder="survey.settings.from"
                date={this.state.from} configs={{firstDay:1, minDate: new Date()}}
                handleChange={this.changeDate}
                inputRef={() => this.from || (element => this.from = element)} />

            <DatePicker classDiv="to" name="survey[to]" datePlaceholder="survey.settings.to"
                date={this.state.to} configs={{firstDay:1, minDate: new Date()}}
                handleChange={this.changeDate}
                inputRef={() => this.to || (element => this.to = element)} />

            <div className="small-12 column">
                <label>{this.props.intl.formatMessage({id: 'survey.settings.status'})}</label>
                <select name="survey[status]" className="margin-none" value={this.state.status} onChange={this.changeStatus}>
                    <option value="1">{this.props.intl.formatMessage({id: "survey.status.1"})}</option>
                    <option value="2">{this.props.intl.formatMessage({id: "survey.status.2"})}</option>
                </select>
            </div>

            <div className="small-12 column clearfix float-left">
                <button className="button m-t" title={this.props.intl.formatMessage({id: 'save'})} >
                    {this.props.intl.formatMessage({id: 'save'})}
                </button>{" "}
                <button className="button hollow close m-t" onClick={this.closeReveal} title={this.props.intl.formatMessage({id: 'cancel'})}>
                    {this.props.intl.formatMessage({id: 'cancel'})}
                </button>
            </div>
        </RevealForm>
    }
}

SurveyMainSettingsForm.contextType = ErrorBoundaryContext;

SurveyMainSettingsForm.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}),
    setSurveyConfig: PropTypes.func,
    id: PropTypes.number,
    title: PropTypes.string,
    from: PropTypes.string,
    to: PropTypes.string,
    status: PropTypes.number,
    open: PropTypes.bool.isRequired,
    closeEditReveal: PropTypes.func.isRequired
};

SurveyMainSettingsForm.defaultProps = {
    setSurveyConfig: null,
    title: '',
    from: new Date().toDateString(),
    to: new Date().toDateString(),
    status: 1
};

export default injectIntl(withRouter(SurveyMainSettingsForm));
