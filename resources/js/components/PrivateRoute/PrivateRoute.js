import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { injectIntl, IntlProvider } from "react-intl";
import PropTypes from "prop-types";

const PrivateRoute = ({ intl, children, user, redirectTo, component: Component, role, ...props }) => {

    if ( role !== user.role ) {
        return <Redirect to={{
            pathname: redirectTo,
            state: {setStickySurveyState: {type: 'alert', message: intl.formatMessage({id: 'errors.private.route'})}}
        }}/>
    }

    return (
        <Route render={Component} {...props}>
            {children}
        </Route>
    );
};

PrivateRoute.propTypes = {
    intl: PropTypes.shape({intl: IntlProvider.propTypes}).isRequired,
    user: PropTypes.object
};

PrivateRoute.defaultProps = {
  redirectTo: '/'
};

export default injectIntl(PrivateRoute);
