import React, {useContext, useState} from 'react'
import { BrowserRouter } from 'react-router-dom';
import { IntlProvider } from 'react-intl';

import Layout from '../Layout/Layout';
import i18n_uk  from '../../i18n/uk';
import i18n_uk_datepicker  from '../../i18n/uk_datepicker';
import ErrorBoundary from "../../components/ErrorBoundary/ErrorBoundary";
import StickyMessageSurveyContext from "../../contexts/stickyMessageSurveyContext";
import UserContext from "../../contexts/userContext";
import BigCircleLoader from "../../components/Loader/BigCircleLoader";

const App = () => {
    const messages = { 'uk': i18n_uk };
    $.datepicker.regional['uk'] = i18n_uk_datepicker;

    const language = document.querySelector('html').getAttribute('lang');

    $.datepicker.setDefaults($.datepicker.regional[language]);

    const [ stickySurveyState, setStickySurveyState ] = useState(useContext(StickyMessageSurveyContext));
    const [ userState, setUserState ] = useState(useContext(UserContext));

    return (
        <IntlProvider locale={language} messages={messages[language]}>
            <ErrorBoundary>
                <BigCircleLoader>
                    <UserContext.Provider value={{userState, setUserState}}>
                        <StickyMessageSurveyContext.Provider value={{stickySurveyState, setStickySurveyState}}>
                            <BrowserRouter>
                                <Layout />
                            </BrowserRouter>
                        </StickyMessageSurveyContext.Provider>
                    </UserContext.Provider>
                </BigCircleLoader>
            </ErrorBoundary>
        </IntlProvider>
    )
};

export default App;
