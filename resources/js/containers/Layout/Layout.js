import React, { useContext, useEffect, lazy, Suspense } from 'react';
import { Switch, Route } from 'react-router-dom'
import { TransitionGroup, CSSTransition } from "react-transition-group";

import ThreeCircleLoader from "../../components/Loader/ThreeCircleLoader";
import Surveys from "../../components/Survey/Surveys";
import StickyMessage from "../../components/Sticky/StickyMessage";
import useDataApi from "../../utils/hooks/useDataApi";
import userContext from "../../contexts/userContext";
import PrivateRoute from "../../components/PrivateRoute/PrivateRoute";
const Survey = lazy(() => import("../../components/Survey/Survey"));
const SurveyUpdate = lazy(() => import( "../../components/Survey/SurveyUpdate"));
const SurveyResult = lazy(() => import("../../components/Survey/SurveyResult/SurveyResult"));

const Layout = () => {

    const { rawData: userRawData, isLoading: isUserLoading, isError: isUserError } = useDataApi({
        url: '/user'
    });

    const { userState, setUserState } = useContext(userContext);
    const user = userRawData && !isUserError ? userRawData : userState;

    useEffect(() => {
        setUserState({...user, isAdmin: user.role === 'admin' && !!user.user_groups.length});
    }, [isUserLoading]);

    return (
        <>
            <StickyMessage/>
            {isUserLoading ?
                <ThreeCircleLoader/>
                :
                <main className="row">
                    <Route render={({ location }) => (
                        <TransitionGroup>
                            <CSSTransition
                                key={location.pathname}
                                classNames="tab"
                                in={true}
                                mountOnEnter={true}
                                appear={false}
                                enter={false}
                                timeout={300}
                            >
                                <div className="tab">
                                    <Switch location={location}>
                                        <Route exact path="/">
                                            <Surveys/>
                                        </Route>
                                        <Route exact path="/surveys/:id([0-9]+)" >
                                            <Suspense fallback={<ThreeCircleLoader/>}>
                                                <Survey />
                                            </Suspense>
                                        </Route>
                                        <PrivateRoute exact path="/surveys/edit/:id([0-9]+)" role={"admin"} user={user}>
                                            <Suspense fallback={<ThreeCircleLoader/>}>
                                                <SurveyUpdate />
                                            </Suspense>
                                        </PrivateRoute>
                                        <PrivateRoute exact path="/surveys/result/:id([0-9]+)" role={"admin"} user={user}>
                                            <Suspense fallback={<ThreeCircleLoader/>}>
                                                <SurveyResult />
                                            </Suspense>
                                        </PrivateRoute>
                                        <Route exact path="" render={() => (window.location = "/404")}/>
                                    </Switch>
                                </div>
                            </CSSTransition>
                        </TransitionGroup>
                    )}/>
                </main>
            }
        </>
    )
};

export default Layout;
