<?php

if ( !defined('CABINET_URL') ) {
    define('CABINET_URL', 'https://cabinet.sumdu.edu.ua');
}

if ( !defined('IIS_URL') ) {
    define('IIS_URL', 'https://iis.sumdu.edu.ua');
}

return [
    'url' => CABINET_URL,

    'cache_hour_lifetime' => 3600,
    'cache_day_lifetime' => 86400,
    'get_person_api' => CABINET_URL.'/api/getperson',
    'get_person_info' => CABINET_URL.'/api/getpersoninfo',
    'get_person_photo' => CABINET_URL.'/api/getpersonphoto',
    'logout_api' => CABINET_URL.'/api/logout',
    'asu_get_specialties' => IIS_URL.'/api/getprofessions',
    'asu_get_groups' => IIS_URL.'/api/getgroups',
    'asu_get_departments' => IIS_URL.'/api/getdivisions',
    'asu_key' => env('CABINET_ASU_KEY', null),
    'service_token' => env('CABINET_SERVICE_TOKEN', null),

    'service_image' => 'app/public/img/survey.svg',
    'encoding' => 'windows-1251',
    'modes' => [
        2 => 2, // image icon service
        3 => 3, // description service
        4 => 4, // dynamic image icon service
        100 => 100, // supported modes
        101 => 101 // logout from service
    ]
];
