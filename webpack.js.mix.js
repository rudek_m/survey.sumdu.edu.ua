const mix = require('laravel-mix');
const ChunkRenamePlugin = require('webpack-chunk-rename-plugin');
require('laravel-mix-merge-manifest');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.react('resources/js/app.js', 'public/js')
    .extract(['react'])
    .version()
    .options({
        terser: {
            extractComments: false,
        }
    })
    .webpackConfig({
         output: {
             publicPath: '/js/',
             chunkFilename: '[name].js?id=[chunkhash]'
         },
        plugins: [
            new ChunkRenamePlugin({
                asyncChunks: '/js/[name].js'
            }),
        ],
        optimization: {
            runtimeChunk: {
                name: '/js/manifest',
            },
            occurrenceOrder: false,
            splitChunks: {
                cacheGroups: {
                    vendor: {
                        name: '/js/vendor',
                        chunks: 'all',
                        test: new RegExp(`(?<!node_modules.*)[\\\\/]node_modules[\\\\/](react)[\\\\/]`, 'i'),
                        enforce: true,
                        priority: 20
                    }
                }
            }
        }
    }).mergeManifest();
