<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/cache/clear/{key}/{tags}', 'IndexController@cacheClear');
Route::middleware('auth.cabinet')->get('/photo', 'IndexController@photo');
Route::middleware('auth.cabinet')->get('/logout', 'IndexController@logout');
Route::middleware(['auth.cabinet', 'role:admin'])->any('adminer', '\Aranyasen\LaravelAdminer\AdminerController@index');
Route::get('/{path}', 'IndexController@index')->where('path','(surveys(.*)?)');
