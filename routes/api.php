<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth.cabinet'])->group(function() {
    Route::get('/user', 'UserController@show');
    Route::post('/answers/survey/{idSurvey}', 'AnswerController@store');

    Route::apiResource('surveys', 'SurveyController', ['parameters' => ['surveys' => 'id']])->only(['index','show']);

    Route::middleware('role:admin')->group(function(){
        Route::apiResource('surveys', 'SurveyController', ['parameters' => ['surveys' => 'id']])->except(['index','show']);
        Route::post('/surveys/copy/{id}', 'SurveyController@copy');

        Route::get('/surveys/result/export/{id}', 'SurveyController@export');

        Route::get('/cabinet/autocomplete/{contingentName}', 'CabinetController@autocomplete');

        Route::apiResource('contingents', 'ContingentController', ['parameters' => ['contingents' => 'id_cont']])->except(['index','show']);

        Route::apiResource('questions', 'QuestionController', ['parameters' => ['questions' => 'id_ques']])->except(['index', 'show']);

    });
});
