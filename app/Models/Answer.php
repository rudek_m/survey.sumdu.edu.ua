<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public $timestamps = false;
    protected $fillable = ['id_survey', 'guid_user', 'id_ques', 'id_var'];

    public function variant()
    {
        return $this->belongsTo(Variant::class, 'id_var');
    }
}
