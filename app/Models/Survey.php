<?php

namespace App\Models;

use App\Builders\SurveyBuilder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class Survey extends Model
{
    protected $hidden = ['created_at', 'updated_at', 'created_user', 'updated_user'];
    protected $fillable = ['title', 'from', 'to', 'status', 'created_user', 'updated_user'];
    protected $appends = ['status_name'];
    protected $casts = ['status' => 'integer'];
    public $timestamps = true;

    public function newEloquentBuilder($query)
    {
        return new SurveyBuilder($query);
    }

    public function contingents()
    {
        return $this->hasMany(Contingent::class, 'id_survey');
    }

    public function questions()
    {
        return $this->hasMany(Question::class, 'id_survey');
    }

    public function variants()
    {
        return $this->hasManyThrough("App\Models\Variant", "App\Models\Question", "id_survey", "id_ques", "id");
    }

    public function answers()
    {
        return $this->hasMany(Answer::class, 'id_survey' );
    }

    public function user_survey() {
        return $this->hasMany(UserSurvey::class, 'id_survey');
    }

    public function user_groups() {
        return $this->hasMany(UserGroup::class, 'guid_usr', 'created_user' );
    }

    public function groups() {
        return $this->hasManyThrough('App\Models\Group', 'App\Models\UserGroup', 'guid_usr', 'id', 'created_user', 'id_grp');
    }

    public function setFromAttribute($value)
    {
        $this->attributes['from'] = Carbon::createFromFormat('d.m.Y', $value)->format('Y-m-d');
    }

    public function setToAttribute($value)
    {
        $this->attributes['to'] = Carbon::createFromFormat('d.m.Y', $value)->format('Y-m-d');
    }

    public function getStatusNameAttribute()
    {
        return trans('labels.status.'.$this->attributes['status']);
    }
}
