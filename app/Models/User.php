<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements AuthenticatableContract
{
    protected $primaryKey = 'guid';
    public $incrementing = false;
    protected $fillable = ['guid'];
    public $timestamps = false;
    public $person;

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return 'guid';
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->{$this->getAuthIdentifierName()};
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->{$this->getAuthIdentifierName()};
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        // TODO: Implement getRememberToken() method.
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        // TODO: Implement setRememberToken() method.
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        // TODO: Implement getRememberTokenName() method.
    }

    public function groups()
    {
        return $this->hasManyThrough('App\Models\Group', 'App\Models\UserGroup', 'guid_usr', 'id', 'guid', 'id_grp');
    }

    public function user_groups() {
        return $this->hasMany(UserGroup::class, 'guid_usr', 'guid' );
    }

    /**
     * Check does user have admin rights.
     *
     * @return boolean
     */
    public function isAdmin() {
        return $this->role === 'admin';
    }
}
