<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contingent extends Model
{
    protected $fillable = [
        'id_categ1', 'name_specialty', 'id_specialty', 'name_degree', 'id_degree', 'name_group',
        'id_form', 'name_form', 'id_kod_std', 'name_kod_std',
        'id_group', 'name_course', 'id_course', 'id_categ2', 'main_post', 'not_include_subdep', 'name_div', 'id_div', 'id_survey'
    ];
    protected $casts = [
        'id_categ1' => 'integer', 'id_specialty' => 'integer', 'id_degree' => 'integer', 'id_form' => 'integer', 'id_kod_std'=> 'integer',
        'id_group' => 'integer', 'id_course' => 'integer', 'id_categ2' => 'integer', 'id_div' => 'integer', 'id_survey' => 'integer',
        'main_post' => 'integer', 'not_include_subdep' => 'integer'
    ];

    public $timestamps = false;

    public function survey() {
        return $this->belongsTo(Survey::class,'id_survey');
    }
}
