<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    protected $fillable = ['text', 'id_ques'];
    public $timestamps = false;

    public function question()
    {
        return $this->belongsTo(Question::class,'id_ques');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class, 'id_var');
    }
}
