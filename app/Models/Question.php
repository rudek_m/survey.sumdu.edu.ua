<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['text', 'type', 'id_survey'];
    protected $casts = ['id_survey' => 'integer'];
    public $timestamps = false;

    public function survey()
    {
        return $this->belongsTo(Survey::class, 'id_survey');
    }

    public function variants()
    {
        return $this->hasMany(Variant::class,'id_ques');
    }
}
