<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSurvey extends Model
{
    protected $table = 'user_survey';
    protected $fillable = [
        'id_survey',
        'guid_user',
        'id_kod_std', 'name_kod_std',
        'id_div', 'name_div', 'code_div',
        'id_specialty', 'name_specialty', 'code_specialty',
        'id_edu_prog', 'name_edu_prog', 'code_edu_prog',
        'id_group', 'name_group',
        'id_degree', 'name_degree',
        'id_form', 'name_form',
        'id_course'
    ];
    public $timestamps = false;


}
