<?php

namespace App\Rules;

use App\Exceptions\NoCredentialsException;
use App\Filters\Survey\SurveyContingentFilter;
use App\Services\SurveyService;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class AnswersRule implements Rule
{
    private String $message;

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param $questions
     * @return bool
     * @throws NoCredentialsException
     */
    public function passes($attribute, $questions)
    {
        $request = app('request');
        $idSurvey = $request->route('idSurvey');
        $request->merge(['data' => 'survey']);

        $surveyService = app(SurveyService::class);
        $survey = $surveyService->getById($idSurvey, $request);
        $request->merge(['survey' => $survey]);

        if ( $survey['status'] !== 2 )
        {
            $this->message = trans('messages.survey.not_active');
            return false;
        }

        if (Carbon::parse($survey['to'])->lt(Carbon::createMidnightDate()))
        {
            $this->message = trans('messages.survey.completed');
            return false;
        }

        if ($survey['answers_exist'])
        {
            $this->message = trans('messages.survey.passed');
            return false;
        }

        if ( !app(SurveyContingentFilter::class)->isAccessedByContingent($survey['contingents']) )
        {
            $this->message = trans('messages.survey.access_denied');
            return false;
        }

        $survey = $survey->toArray();

        if ( count($survey['questions']) !== count($questions) ) {
            $this->message = trans('validation.custom.questions.answers.invalid_count_questions');
            return false;
        }

        if ( count( array_diff(array_column($survey['questions'],'id'), array_keys($questions)) ) !== 0) {
            $this->message = trans('validation.custom.questions.answers.invalid_questions');
            return false;
        }

        $index = 1;
        foreach ( $survey['questions'] as $question ) {
            if ($question['type'] === 'single' && count($questions[$question['id']]) > 1 ) {
                $this->message =  trans('validation.custom.questions.answers.invalid_count_answers');
                return false;
            }

            $variants = array_column($question['variants'],'id');
            if ( in_array($question['type'], ['single', 'multi']) ) {
                foreach ( $questions[$question['id']] as $idVariant ) {
                    if ( !in_array($idVariant, $variants) ) {
                        $this->message =  trans('validation.custom.questions.answers.invalid_answers');
                        return false;
                    }
                }
            }

            if ( $question['type'] === 'text' ) {
                if ( empty($questions[$question['id']]['id_var']) ) {
                    $this->message = trans('validation.custom.questions.answers.invalid_extended_answer');
                    return false;
                }

                if ( !in_array($questions[$question['id']]['id_var'], $variants) ) {
                    $this->message = trans('validation.custom.questions.answers.invalid_answers');
                    return false;
                }

                if ( empty($questions[$question['id']]['text']) ) {
                    $this->message = sprintf(trans('validation.custom.questions.answers.empty_extended_answer'), $index);
                    return false;
                }

                if ( mb_strlen($questions[$question['id']]['text']) > 500 ) {
                    $this->message = sprintf(trans('validation.custom.questions.answers.oversize_extended_answer'), $index);
                    return false;
                }

            }
            $index++;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
