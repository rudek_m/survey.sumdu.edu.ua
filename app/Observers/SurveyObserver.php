<?php

namespace App\Observers;

use App\Models\Survey;
use Illuminate\Support\Facades\Cache;

class SurveyObserver
{
    function deleting(Survey $contingent) {
        Cache::store('apc')->tags('surveys_count')->clear();
        return true;
    }
}
