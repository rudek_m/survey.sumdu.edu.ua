<?php

namespace App\Observers;

use App\Models\Contingent;
use App\Models\Survey;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ContingentObserver
{
    function saving(Contingent $contingent) {
        Survey::findOrFail($contingent['id_survey'])->update([
            'updated_at' => Carbon::now(),
            'updated_user' => Auth::user()->guid
        ]);
        return true;
    }

    function deleting(Contingent $contingent) {
        Survey::findOrFail($contingent['id_survey'])->update([
            'updated_at' => Carbon::now(),
            'updated_user' => Auth::user()->guid
        ]);
        return true;
    }
}
