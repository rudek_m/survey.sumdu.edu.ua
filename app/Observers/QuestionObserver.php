<?php

namespace App\Observers;

use App\Models\Answer;
use App\Models\Question;
use App\Models\UserSurvey;
use App\Models\Survey;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class QuestionObserver
{
    function saving(Question $question) {
        Answer::where('id_survey', $question['id_survey'])->delete();
        UserSurvey::where('id_survey', $question['id_survey'])->delete();
        Survey::findOrFail($question['id_survey'])->update([
            'updated_at' => Carbon::now(),
            'updated_user' => Auth::user()->guid
        ]);
        $question->variants()->delete();
        return true;
    }

    function deleting(Question $question) {
        Answer::where('id_survey', $question['id_survey'])->delete();
        UserSurvey::where('id_survey', $question['id_survey'])->delete();
        Survey::findOrFail($question['id_survey'])->update([
            'updated_at' => Carbon::now(),
            'updated_user' => Auth::user()->guid]);
        return true;
    }
}
