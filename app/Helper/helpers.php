<?php

if (!function_exists('cabinet_message')) {

    /**
     * Urlencode and change encoding for text message for cabinet.
     *
     * @param $message
     * @return string
     */
    function cabinet_message($message)
    {
        return urlencode(iconv(config('app.encoding'), config('cabinet.encoding'), $message));
    }
}

if(!function_exists('filter_html_tags_spaces')) {

    function filter_html_tags_spaces($field)
    {
        return preg_replace('/[\t\n\r\s]+/', ' ', htmlspecialchars(strip_tags($field)));
    }
}

if(!function_exists('filter_tags_spaces')) {

    function filter_tags_spaces($field)
    {
        return preg_replace('/[\t\n\r\s]+/', ' ', strip_tags($field));
    }
}
if (!function_exists('icon_service_response')) {
    function icon_service_response($count = 0) {
        $view = 'survey';
        $data = ['count' => $count];
        if ($count > 0) {
            $view = 'surveys_count';
        }

        return response()->
            view('svgs.'.$view, $data, 200)->
            header('Content-Type', 'image/svg+xml');
    }
}
