<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\ResponseFactory;

class ApiResponseServiceProvider extends ServiceProvider {

    public function boot(ResponseFactory $factory)
    {
        $factory->macro('success', function ($data = null, string $message = null, $status = 200, $headers = []) use ($factory) {
            $format = [
                'status' => 'success',
                'message' => $message,
                'data' => $data,
            ];

            return $factory->json($format, $status, $headers);
        });

        $factory->macro('error', function (string $message = '', $status = 400, array $errors = [], $headers = []) use ($factory) {
            $format = [
                'status' => 'error',
                'message' => $message,
                'errors' => $errors

            ];

            return $factory->json($format, $status, $headers);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
