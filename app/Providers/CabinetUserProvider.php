<?php


namespace App\Providers;


use App\Exceptions\CabinetResponseException;
use App\Models\User;
use App\Repositories\CabinetRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Validator;

class CabinetUserProvider implements UserProvider
{

    private $user;
    private $cabinetRepository;

    public function __construct(User $user, CabinetRepository $cabinetRepository)
    {
        $this->user = $user;
        $this->cabinetRepository = $cabinetRepository;
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        // TODO: Implement retrieveById() method.
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param mixed $identifier
     * @param string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        // TODO: Implement retrieveByToken() method.
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param string $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        // TODO: Implement updateRememberToken() method.
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     * @throws CabinetResponseException
     * @throws \Illuminate\Http\Client\RequestException
     */
    public function retrieveByCredentials(array $credentials = [])
    {
        $auth_response = $this->cabinetRepository->gerPersonInfo($credentials['key'], $credentials['sid']);
        $this->validateCredentials($this->user, is_array($auth_response) ? $auth_response : []);
        return $auth_response['result'];
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param array $credentials
     * @return bool
     * @throws CabinetResponseException
     */
    public function validateCredentials(Authenticatable $user, array $credentials = [])
    {
        $validator = Validator::make($credentials, [
            'status'=> 'required|in:OK,ERROR_CABINET',
            'result' => 'required'
        ])->sometimes('result.guid', 'required', function($response) {
            return $response['status'] === 'OK';
        });

        if ($validator->fails()) {
            throw new CabinetResponseException($validator->errors()->first());
        }

        if ($credentials['status'] === 'ERROR_CABINET') {
            throw new CabinetResponseException($credentials['result']);
        }

        return true;
    }
}
