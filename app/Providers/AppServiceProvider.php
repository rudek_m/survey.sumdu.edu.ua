<?php

namespace App\Providers;

use App\Models\Contingent;
use App\Models\Question;
use App\Models\Survey;
use App\Observers\ContingentObserver;
use App\Observers\QuestionObserver;
use App\Observers\SurveyObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Question::observe(QuestionObserver::class);
        Contingent::observe(ContingentObserver::class);
        Survey::observe(SurveyObserver::class);
    }
}
