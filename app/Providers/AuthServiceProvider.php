<?php

namespace App\Providers;

use App\Services\Auth\CabinetGuard;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('cabinet', function ($app, array $config) {
            return new CabinetUserProvider($app->make('App\Models\User'), $app->make('App\Repositories\CabinetRepository'));
        });

        Auth::extend('cabinet', function($app, $name, array $config) {
            return new CabinetGuard(Auth::createUserProvider($config['provider']), $app->make('request'));
        });
    }
}
