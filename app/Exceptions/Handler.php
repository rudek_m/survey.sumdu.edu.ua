<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Client\ConnectionException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof ValidationException) {
            $errors = $this->formatValidateError($exception->validator);
            $first = '';
            if (!empty($errors) && !empty($errors[0])) {
                $first = $errors[0]['message'];
            }

            return response()->error($first, 422);
        } else if ($exception instanceof ConnectionException || $exception instanceof RequestException) {
            return response()->redirectTo(config('cabinet.url').'?error='.cabinet_message(trans('messages.cabinet.data_error') . ' ' . $exception->getMessage()));
        } else if ($exception instanceof NoCredentialsException) {
            return response()->error($exception->getMessage(), 403);
        } else if ($exception instanceof TokenMismatchException ) {
            return response()->error(trans('messages.csrf.mismatch'), 419);
        }

        return parent::render($request, $exception);
    }

    protected function formatValidateError($validator) {
        $errors = $validator->errors()->toArray();
        $return = [];
        foreach ($errors as $field => $message) {
            $r = ['field' => $field];
            foreach ($message as $key => $msg) {
                if ($key) {
                    $r['message'.$key] = $msg;
                } else {
                    $r['message'] = $msg;
                }
            }
            $return[] = $r;
        }
        return $return;
    }
}
