<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CabinetAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( !Auth::check() ) {
            if ( $request->ajax() ) {
                return response()->error(trans('auth.failed'), 401);
            }
            return redirect()->away(config('cabinet.url').'?error='.cabinet_message(trans('auth.failed')));
        }

        return $next($request);
    }
}
