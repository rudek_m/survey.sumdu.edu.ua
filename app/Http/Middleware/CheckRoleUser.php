<?php

namespace App\Http\Middleware;

use App\Filters\Survey\SurveyAdminGroupFilter;
use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Survey;
use Illuminate\Database\Eloquent\Builder;

class CheckRoleUser
{

    private function responseRedirect($request, $messageLabel)
    {
        if ( $request->ajax() ) {
            return response()->error(trans($messageLabel), 403);
        }
        return redirect()->away(config('cabinet.url').'?error='.cabinet_message(trans($messageLabel)));
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param array $roles
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $user = Auth::user();
        if ( in_array($user['role'],  $roles) )
        {
            if (!app(SurveyAdminGroupFilter::class, [$request])->isAccessible())
            {
                return $this->responseRedirect($request, 'auth.forbidden_group');
            }
        }
        else
        {
            return $this->responseRedirect($request, 'auth.forbidden');
        }

        return $next($request);
    }
}
