<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Session\Session;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomSessionStart extends StartSession
{

    private Request $request;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->request = $request;
        $modes = collect(config('cabinet.modes'))->filter(fn($value) => $value !== 101 && $value !== 4);
        if ( $modes->contains($this->request->query('mode', null)) || $this->request->is('cache/clear/*') ) {
            config()->set('session.driver', 'array');
        }
        return parent::handle($request, $next);

    }

    protected function addCookieToResponse(Response $response, Session $session)
    {
        $modes = collect(config('cabinet.modes'))->filter(fn($value) => $value !== 4);
        if ( !$modes->contains($this->request->query('mode', null)) || !$this->request->is('cache/clear/*') ) {
            parent::addCookieToResponse($response, $session);
        }
    }
}
