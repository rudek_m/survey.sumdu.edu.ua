<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;
use Closure;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'adminer'
    ];

    public function handle($request, Closure $next)
    {
        if ( !collect(config('cabinet.modes'))->contains($request->query('mode', null)) ) {
            return parent::handle($request, $next);
        }
        return $next($request);
    }

    protected function addCookieToResponse($request, $response)
    {
        $request->session()->regenerateToken();
        return parent::addCookieToResponse($request, $response);
    }
}
