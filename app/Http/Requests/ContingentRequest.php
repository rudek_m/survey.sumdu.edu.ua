<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContingentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "id_survey" => "required|integer",

            "categ1" => "array|required_without:categ2|max:3|min:1",
            "categ1.*" => "integer|in:2,4,8",

            "id_specialty" => "nullable|required_with:name_specialty|integer",
            "name_specialty" => "nullable|required_with:id_specialty|string",

            "id_degree" => "nullable|required_with:name_degree|integer",
            "name_degree" => "nullable|required_with:id_degree|string",

            "id_form" => "nullable|required_with:name_form|integer",
            "name_form" => "nullable|required_with:id_form|string",

            "id_kod_std" => "nullable|required_with:name_std|integer",
            "name_kod_std" => "nullable|required_with:kod_std|string",

            "id_group" => "nullable|required_with:name_group|integer",
            "name_group" => "nullable|required_with:id_group|string",

            "id_course" => "nullable|required_with:name_course|integer",
            "name_course" => "nullable|required_with:id_course|string",

            "categ2" => "array|required_without:categ1|max:3|min:1",
            "categ2.*" => "integer|in:2,4,8",

            "name_div" => "nullable|required_with:id_div|string",
            "id_div" => "nullable|required_with:name_div|integer",

            "main_post" => "nullable|integer|in:1",
            "not_include_subdep" => "nullable|integer|in:1"
        ];
    }

    public function withValidator($validator) {
        $request = $this;

        $validator->after(function($validator) use ($request) {
            $contingentTypes =  ['categ1', 'categ2'];
            array_walk( $contingentTypes,
                function($contingent) use ($request) {
                    isset($request->{$contingent}) ?
                        $request->merge(['id_' . $contingent => array_sum(array_unique($request->{$contingent}))]) :
                        $request->merge(['id_' . $contingent => 0]);
                }
            );
            if ( empty($request->main_post) ) {
                $request->merge(['main_post' => 0]);
            }
            if ( empty($request->not_include_subdep) ) {
                $request->merge(['not_include_subdep' => 0]);
            }
        });
    }
}
