<?php

namespace App\Http\Requests;

use App\Rules\AnswersRule;
use Illuminate\Foundation\Http\FormRequest;

class AnswersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if (is_array($this->questions)) {
            $questions = [];
            foreach($this->questions as $key => $answers) {
                $questions[$key] = array_map(
                    fn($answer) => filter_tags_spaces($answer),
                    array_unique($answers)
                );
            }
            $this->replace(['questions' => $questions]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'questions' => ['required', 'array', new AnswersRule]
        ];
    }
}
