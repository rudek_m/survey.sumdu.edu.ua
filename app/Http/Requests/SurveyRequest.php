<?php


namespace App\Http\Requests;


use App\Filters\Survey\SurveyAdminGroupFilter;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SurveyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data' => 'sometimes|string|in:survey,answers,edit,result',
        ];
    }

    public function withValidator($validator) {
        $request = $this;
        $validator->after(function($validator) use ($request)
        {
            if ( in_array($request->query('data'), ['survey', 'edit', 'result']) )
            {
                $user = Auth::user();
                if ( $user['role'] === 'admin' )
                {
                    if (!app(SurveyAdminGroupFilter::class, [$request])->isAccessible())
                    {
                        $validator->errors()->add('', trans('auth.forbidden_group'));
                    }
                }
                else
                {
                    $validator->errors()->add('', trans('messages.survey.data.access'));
                }
            }
        });
    }
}
