<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SurveyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function prepareForValidation()
    {
        if (isset($this->survey) && isset($this->survey['title'])) {
            $survey = $this->survey;
            $survey['title'] = filter_html_tags_spaces($survey['title']);
            $this->replace(['survey' => $survey]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'survey' => 'required|array',
            'survey.title' => 'required|string|max:400',
            'survey.from' => ['required','date_format:d.m.Y'/*,'after_or_equal:'.date('d.m.Y')*/],
            'survey.to' => 'required|date_format:d.m.Y|after_or_equal:survey.from',
            'survey.status' => 'required|integer'
        ];
    }
}
