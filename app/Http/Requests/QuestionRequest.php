<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function prepareForValidation()
    {
        if ( isset($this->question['text']) ) {
            $this->merge(['text' => filter_html_tags_spaces($this->question['text'])]);
        }
        if ( isset($this->variants) && is_array($this->variants) ) {
            $this->merge([
                'variants' => array_map(
                    fn($variant) => filter_html_tags_spaces($variant),
                    $this->variants
                )]
            );
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question' => 'required|array',
            'question.text' => 'required|string|max:400',
            'question.type' => 'required|in:single,multi,text',
            'question.id_survey' => 'required|integer',
            'variants' => 'required_if:question.type,single,multi|array|min:2|max:20',
            'variants.*' => 'required_with:variants|string|max:250'
        ];
    }
}
