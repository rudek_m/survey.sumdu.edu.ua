<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use App\Repositories\CabinetRepository;
use App\Services\SurveyService;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;


use App\Exceptions\CabinetAuthKeyException;
use App\Exceptions\CabinetResponseException;

class IndexController extends Controller
{

    /**
     * Cabinet authentication, cabinet api response and main page.
     *
     * @param Request $request
     * @param Guard $cabinetGuard
     * @param SurveyService $surveyService
     * @return View
     */
    public function index(Request $request, Guard $cabinetGuard, SurveyService $surveyService)
    {
        $mode = $request->query('mode', 0);

        $modes = config('cabinet.modes');
        if ( !in_array($mode, array_filter($modes, fn($value) => $value !== 4)) ) {
            try {
                $cabinetGuard->validate(['key' => $request->query('key', []), 'sid' => session()->getId()]);
            } catch (CabinetResponseException | CabinetAuthKeyException $e) {
                if ( $mode == 4 ) return icon_service_response();
                return redirect()->away(config('cabinet.url').'?error='.cabinet_message($e->getMessage()));
            }
        }

        switch($mode) {
            case $modes[2]:
                return icon_service_response();
            case $modes[3]:
                return response(trans('labels.head.description_shot'),200)->header('Content-type', 'text/plain');
            case $modes[4]:
                return icon_service_response($surveyService->countAll());
            case $modes[100]:
                return response('', 200)->header('X-Cabinet-Support',29);
            case $modes[101]:
                $sid = $request->query('sid');
                if ( $sid ) {
                    session()->setId($sid);
                    session()->start();
                    session()->invalidate();
                    exit();
                }
        }

        return view('layout');
    }

    public function photo(CabinetRepository $cabinetRepository)
    {
        $response = $cabinetRepository->getPersonPhoto(session('key'));

        if ( $response->header('Content-Type') !== 'application/json' ) {
            return response($response->body())->withHeaders([
                'Content-type' => $response->header('Content-Type'),
                'Content-length' => $response->header('Content-Length')
            ]);
        }
    }

    public function logout(CabinetRepository $cabinetRepository)
    {
        $key = session('key');
        session()->invalidate();
        $response = $cabinetRepository->logout($key);

        if ( isset($response['status']) && $response['status'] === 'OK' ) {
            return redirect()->away(config('cabinet.url'));
        }  else {
            return redirect()->away(config('cabinet.url').'?error='.cabinet_message(config('messages.cabinet.error_logout')));
        }
    }

    public function cacheClear($key, $tags) {
        if ( $key === config('cache.key') ) {
            $count = Survey::query()->
                where('updated_at', '>=', Carbon::now()->subHours(24)->toDateTimeString())->
                count();
            if ( $count > 0 ) return Cache::store('apc')->tags(explode(',', $tags))->clear();
            return false;
        }
    }
}
