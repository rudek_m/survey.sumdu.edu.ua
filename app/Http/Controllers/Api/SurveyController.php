<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\NoCredentialsException;
use App\Exports\SurveyResultExport;
use App\Http\Requests\SurveyRequest;
use App\Http\Requests\SurveyStoreRequest;
use App\Models\Survey;
use App\Services\SurveyService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class SurveyController extends Controller
{
    private SurveyService $service;

    public function __construct(SurveyService $surveyService)
    {
        $this->service = $surveyService;
    }

    /**
     * Display a listing of the survey.
     *
     * @return Response
     */
    public function index()
    {
        return response()->success($this->service->all());
    }

    /**
     * Display the specified survey.
     *
     * @param int $id
     * @param SurveyRequest $request
     * @return Response
     * @throws NoCredentialsException
     */
    public function show($id, SurveyRequest $request)
    {
        return response()->success($this->service->getById($id, $request));
    }

    /**
     * Store a newly created survey in storage.
     *
     * @param SurveyStoreRequest $request
     * @return Response
     */
    public function store(SurveyStoreRequest $request)
    {
        return response()->success($this->service->create($request), null, 201);
    }


    /**
     * Update the survey in storage.
     *
     * @param int $id
     * @param SurveyStoreRequest $request
     * @return Response
     */
    public function update($id, SurveyStoreRequest $request)
    {
        return response()->success($this->service->update($id, $request));
    }


    /**
     * Remove the survey from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $result = Survey::findOrFail($id)->delete();
        return response()->success($result, trans('messages.survey.success_remove'));
    }

    /**
     * @param $id
     * @return BinaryFileResponse
     */
    public function export($id) {
        return Excel::download(new SurveyResultExport($id), 'result.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function copy($id) {
        return response()->success($this->service->copy($id), trans('messages.survey.copied'));
    }
}
