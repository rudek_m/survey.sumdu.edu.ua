<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContingentRequest;
use App\Models\Contingent;
use App\Services\ContingentService;

class ContingentController extends Controller
{
    private ContingentService $service;

    public function __construct(ContingentService $service)
    {
        $this->service = $service;
    }

    public function store(ContingentRequest $request)
    {
        return response()->success($this->service->create($request), null, 201);
    }

    public function update($id_cont, ContingentRequest $request) {
        return response()->success($this->service->update($id_cont, $request));
    }

    /**
     * Remove the contingent from storage.
     *
     * @param  int $id_cont
     * @return Response
     */
    public function destroy(int $id_cont)
    {
        $result = Contingent::findOrFail($id_cont)->delete();
        return response()->success($result, trans('messages.contingent.success_remove'));
    }
}
