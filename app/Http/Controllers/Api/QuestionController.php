<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionRequest;
use App\Models\Question;
use App\Services\QuestionService;
use Illuminate\Http\Response;

class QuestionController extends Controller
{
    private QuestionService $service;

    public function __construct(QuestionService $service)
    {
        $this->service = $service;
    }

    /**
     * Store a newly created survey in storage.
     *
     * @param QuestionRequest $request
     * @return Response
     */
    public function store(QuestionRequest $request)
    {
        return response()->success($this->service->create($request), null, 201);
    }

    /**
     *
     *
     * @param $id_ques
     * @param QuestionRequest $request
     * @return mixed
     */
    public function update($id_ques, QuestionRequest $request) {
        return response()->success($this->service->update($id_ques, $request));
    }

    /**
     * Remove the question from storage.
     *
     * @param  int $id_ques
     * @return Response
     */
    public function destroy(int $id_ques)
    {
        $result = Question::findOrFail($id_ques)->delete();
        return response()->success($result, 'Питання успішно видалено.');
    }
}
