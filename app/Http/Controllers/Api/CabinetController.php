<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CabinetService;
use Illuminate\Http\Request;

class CabinetController extends Controller
{

    public function autocomplete(string $contingentName, Request $request, CabinetService $service)
    {
        return response()->success($service->autocomplete($contingentName, $request->get('search','')));
    }
}
