<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\AnswersRequest;
use App\Services\AnswerService;
use App\Http\Controllers\Controller;


class AnswerController extends Controller
{
    protected $answerService;

    public function __construct(AnswerService $answerService)
    {
        $this->answerService = $answerService;
    }

    public function store(AnswersRequest $request, $idSurvey)
    {
        $this->answerService->insertAnswers($request->input('questions'), $idSurvey);
        return response()->success(null, trans('messages.surveys.success'), 201);
    }
}
