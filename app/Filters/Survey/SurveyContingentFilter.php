<?php


namespace App\Filters\Survey;

use App\Repositories\CabinetRepository;

class SurveyContingentFilter
{
    private CabinetRepository $cabinetRepository;
    private array $person;
    private array $specialtiesIdParent = [];
    private array $groupsIdParent = [];
    private array $departmentsIdParent = [];
    private array $info1_allow_state = [
        1, // studying
        4  // graduate
    ];
    private array $info2_allow_state = [
        1,  // working
        //2,  // vacation
        3   // відсутній
    ];

    private array $info2_main_post = [
        1,
        5,  // основне місце роботи, поєднання з навчанням студенти
        6,  //
        10, // основне місце роботи поєднання, з навчанням (докторанти)
        13
    ]; // main post

    private array $personInfo1 = [];
    private array $personInfo2 = [];

    public function __construct(CabinetRepository $cabinetRepository)
    {
        $this->person = auth()->user()->person;
        $this->cabinetRepository = $cabinetRepository;
    }

    protected function attrNotEquals($contingentAttr, $personAttr)
    {
        return $contingentAttr && $contingentAttr !== $personAttr;
    }

    protected function attrHierarchicallyEquals($contingentAttr, $personAttr, $idsParent)
    {
        if ( $contingentAttr && $contingentAttr !== $personAttr ) {
            $idParent = $personAttr;
            while (isset($idsParent[$idParent]) && $idsParent[$idParent] !== 0) {
                $idParent = $idsParent[$idParent];
                if ($idParent === $contingentAttr) {
                    return true;
                }
                if ($idParent === $idsParent[$idParent]) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    protected function checkCategMask($contingent_categ, $info_categ) {
        return (pow(2, $info_categ) & $contingent_categ) > 0 ;
    }

    protected function getIdParent($array, $key, $column = 'ID_PAR') {
        return array_map(fn($item) => intval($item), array_column($array, $column, $key));
    }

    protected function getSpecialtiesIdParent() : array {
        if (!$this->specialtiesIdParent) {
            $this->specialtiesIdParent = $this->getIdParent($this->cabinetRepository->getSpecialties(), 'ID_PROF');
        }
        return $this->specialtiesIdParent;
    }

    protected function getGroupsIdParent() : array {
        if (!$this->groupsIdParent) {
            $this->groupsIdParent = $this->getIdParent($this->cabinetRepository->getGroups(), 'ID_GROUP');
        }
        return $this->groupsIdParent;
    }

    protected function getDepartmentsIdParent() : array {
        if (!$this->departmentsIdParent) {
            $this->departmentsIdParent = $this->getIdParent($this->cabinetRepository->getDepartments(), 'ID_DIV');
        }
        return $this->departmentsIdParent;
    }

    public function isAccessedByContingent($contingents) {
        $isAccess = false;
        foreach($contingents as $contingent) {
            if ($contingent->id_categ1 && isset($this->person['info1'])) {
                foreach ($this->person['info1'] as $info1) {
                    if (in_array($info1['KOD_STATE'], $this->info1_allow_state) ) {
                        // check category for special info1 and contingent
                        if ( !$this->checkCategMask($contingent->id_categ1, $info1['CATEG']) ) {
                            continue;
                        }

                        // check form
                        if ( $this->attrNotEquals($contingent->id_form, intval($info1['KOD_FORM'])) ) {
                            continue;
                        }

                        // check kod_std, foreign student or ukrainian
                        if ( $this->attrNotEquals($contingent->id_kod_std, intval($info1['KOD_STD'])) ) {
                            continue;
                        }

                        // check specialty
                        if ( !$this->attrHierarchicallyEquals($contingent->id_specialty, intval($info1['KOD_PROFS']), $this->getSpecialtiesIdParent())) {
                            continue;
                        }

                        // check group
                        if ( !$this->attrHierarchicallyEquals($contingent->id_group, intval($info1['KOD_GROUP']), $this->getGroupsIdParent()) ) {
                            continue;
                        }

                        // check degree
                        if ( $this->attrNotEquals($contingent->id_degree, intval($info1['KOD_LEVEL'])) ) {
                            continue;
                        }

                        // check course
                        if ( $this->attrNotEquals($contingent->id_course, intval(($info1['NUM_SEM'] + 1) / 2)) ) {
                            continue;
                        }

                        if ( $contingent->not_include_subdep && $contingent->id_div != $info1['KOD_DIV']) {
                            continue;
                        } else if ( !$this->attrHierarchicallyEquals($contingent->id_div, intval($info1['KOD_DIV']), $this->getDepartmentsIdParent()) ) {
                            continue;
                        }

                        $this->personInfo1 = $info1;
                        $isAccess = true;
                        break 2;
                    }
                }
            }

            if ($contingent->id_categ2 && isset($this->person['info2'])) {
                foreach ($this->person['info2'] as $info2) {
                    if (in_array($info2['KOD_STATE'], $this->info2_allow_state)) {
                        if ( $contingent->main_post && !in_array($info2['KOD_SYMP'], $this->info2_main_post) ) {
                            continue;
                        }

                        // check category for special info2 and contingent
                        if ( !$this->checkCategMask($contingent->id_categ2, $info2['CATEG']) ) {
                            continue;
                        }

                        if ( $contingent->not_include_subdep && $contingent->id_div != $info2['KOD_DIV']) {
                            continue;
                        } else if ( !$this->attrHierarchicallyEquals($contingent->id_div, intval($info2['KOD_DIV']), $this->getDepartmentsIdParent()) ) {
                            continue;
                        }

                        $this->personInfo2 = $info2;
                        $isAccess = true;
                        break 2;
                    }
                }
            }

        }
        $request = app('request');
        $request->merge(['info1' => $this->personInfo1, 'info2' => $this->personInfo2]);
        return $isAccess;
    }

}
