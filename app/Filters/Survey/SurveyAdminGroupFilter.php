<?php

namespace App\Filters\Survey;

use App\Models\Survey;
use App\Models\Question;
use App\Models\Contingent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class SurveyAdminGroupFilter
{
    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function isAccessible(): bool
    {
        $idSurveyInputField = collect($this->request->only(['question.id_survey', 'id_survey']))->flatten()->get(0);

        $route = $this->request->route();
        $idRouteParam = null;
        if ( $idSurveyRouteParam = $route->parameter('id') ) {
            $idRouteParam = $idSurveyRouteParam;
        } else if ( $idQuestionRouteParam = $route->parameter('id_ques')) {
            $idRouteParam = Question::query()->select('id_survey')->where('id', $idQuestionRouteParam)->get()->pluck('id_survey')->toArray()[0];
        } else if ( $idContingentRouteParam = $route->parameter('id_cont') ) {
            $idRouteParam = Contingent::query()->select('id_survey')->where('id', $idContingentRouteParam)->get()->pluck('id_survey')->toArray()[0];
        }

        //don't swap condition's expressions each other, because first of all check id_survey in input fields
        if ( ($idSurvey = $idSurveyInputField) || ($idSurvey = $idRouteParam) )
        {
            $user = Auth::user();
            return Survey::query()->whereHas('user_groups', function(Builder $query) use ($user) {
                $query->whereIn('user_groups.id_grp', $user->user_groups->collect()->pluck('id_grp')->toArray());
            })->where('surveys.id', $idSurvey)->exists();
        }
        return true;
    }
}
