<?php


namespace App\Filters\Cabinet;

use Closure;

class SearchByAttributeFilter
{
    private string $search;
    private string $attr;

    public function __construct($search, $attr)
    {
        $this->search = strval($search);
        $this->attr = $attr;
    }

    public function handle($array, Closure $next)
    {
        $array = !empty($this->search) ?
            array_filter($array, fn($value) => is_numeric(mb_stripos($value[$this->attr], $this->search))) :
            array_slice($array, 0, 20);

        return $next($array);
    }
}
