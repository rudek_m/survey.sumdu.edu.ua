<?php


namespace App\Filters\Cabinet;

use Closure;

class FieldsForAutocompleteFilter
{
    private string $id;
    private string $title;
    private string $description;

    public function __construct($id, $title, $description = '')
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
    }

    public function handle($array, Closure $next) {
        $autocompleteResponse = array_map(function($value) {
            $item = ['id' => $value[$this->id], 'name' => $value[$this->title]];
            if ($this->description) {
                $item['description'] = $value[$this->description];
            }
            return $item;
        }, $array);

        return $next(count($autocompleteResponse) ? $autocompleteResponse : ['title' => '']);
    }
}
