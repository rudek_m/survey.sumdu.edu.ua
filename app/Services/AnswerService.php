<?php

namespace App\Services;

use App\Models\Answer;
use App\Models\UserSurvey;
use App\Repositories\CabinetRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Exception;
use Throwable;

class AnswerService
{
    private array $user_survey_attrs = [
        'KOD_DIV' => 'id_div',
        'CODE_DIV' => 'code_div',
        'NAME_DIV' => 'name_div',

        'KOD_GROUP' => 'id_group',
        'NAME_GROUP' => 'name_group',

        'KOD_FORM' => 'id_form',
        'NAME_FORM' => 'name_form',

        'KOD_LEVEL' => 'id_degree',
        'NAME_LEVEL' => 'name_degree',

        'NUM_SEM' => 'id_course',

        'KOD_STD' => 'id_kod_std',

    ];
    private CabinetRepository $cabinetRepository;

    public function __construct(CabinetRepository $cabinetRepository)
    {
        $this->cabinetRepository = $cabinetRepository;
    }

    public function insertAnswers($questions, $idSurvey)
    {
        $user = Auth::user();
        $guidUser = $user['guid'];

        $questionsSurvey = app('request')->survey['questions']->keyBy('id')->toArray();

        $answersInsert = [];
        foreach($questions as $id_ques => $answers) {
            $isOpenQuestion = $questionsSurvey[$id_ques]['type'] === 'text';
            if ($isOpenQuestion) {
                $answersInsert[] = [
                    'guid_user' => $guidUser,
                    'id_survey' => $idSurvey,
                    'id_ques' => $id_ques,
                    'id_var' => $answers['id_var'],
                    'text' => $answers['text']
                ];
            } else {
                foreach ( $answers as $idVariant ) {
                    $answersInsert[] = [
                        'guid_user' => $guidUser,
                        'id_survey' => $idSurvey,
                        'id_ques' => $id_ques,
                        'id_var' => $idVariant,
                        'text' => null
                    ];
                }
            }
        }

        try {
            DB::beginTransaction();

            Answer::where([
                ['guid_user', $guidUser],
                ['id_survey', $idSurvey]
            ])->delete();

            Answer::insert($answersInsert);

            $request = app('request');
            $info = [];
            if ( $request->info1 ) {
                $info = array_combine(array_values($this->user_survey_attrs), array_intersect_key($request->info1, $this->user_survey_attrs));
                $info['id_course'] = intval(($info['id_course'] + 1) / 2);
                $info['name_kod_std'] = collect(trans('labels.contingent.kod_std'))->keyBy('id')[$info['id_kod_std']]['name'];

                $info['id_edu_prog'] = null;
                $info['name_edu_prog'] = null;
                $info['code_edu_prog'] = null;

                $info['id_specialty'] = $request->info1['KOD_PROFS'];
                $info['code_specialty'] = $request->info1['CODE_PROFS'];
                $info['name_specialty'] = $request->info1['NAME_PROFS'];

                $specialties = $this->cabinetRepository->getSpecialties();
                $idParent = intval($request->info1['KOD_PROFS']);
                do {
                    if ( in_array($specialties[$idParent]['KOD_INCL'], [9,10,11]) ) {
                        $info['id_edu_prog'] = $specialties[$idParent]['ID_PROF'];
                        $info['name_edu_prog'] = $specialties[$idParent]['NAME_PROF'];
                        $info['code_edu_prog'] = $specialties[$idParent]['CODE_PROF'];
                    }

                    if ( in_array($specialties[$idParent]['KOD_INCL'], [2]) ) {
                        $info['id_specialty'] = $specialties[$idParent]['ID_PROF'];
                        $info['name_specialty'] = $specialties[$idParent]['NAME_PROF'];
                        $info['code_specialty'] = $specialties[$idParent]['CODE_PROF'];
                        break;
                    }

                    $idParent = intval($specialties[$idParent]['ID_PAR']);
                } while ( isset($specialties[$idParent]) && (intval($specialties[$idParent]['ID_PAR']) !== 0));

            } else if ( $request->info2 ) {
                $this->user_survey_attrs = array_slice($this->user_survey_attrs, 0, 3);
                $info = array_combine(array_values($this->user_survey_attrs), array_intersect_key($request->info2, $this->user_survey_attrs));
            }

            $create = ['id_survey' => $idSurvey, 'guid_user' => $guidUser] + $info;
            UserSurvey::create($create);

            DB::commit();
            Cache::store('apc')->tags(['surveys_count'])->forget($guidUser);
        } catch (Exception | Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
