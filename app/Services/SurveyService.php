<?php

namespace App\Services;

use App\Exceptions\NoCredentialsException;
use App\Filters\Survey\SurveyContingentFilter;
use App\Models\Survey;
use Illuminate\Support\Facades\Cache;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class SurveyService
{

    private array $fields = [
        'survey.title',
        'survey.from',
        'survey.to',
        'survey.status'
    ];

    public function all()
    {
        $user = auth()->user();
        $userGroups = $user->user_groups->collect()->pluck('id_grp')->toArray();
        $person = $user->person;

        $whereSurveyContingents = fn($query) => $query->where([
            ['from', '<=', now()->toDateString()],
            [DB::raw('(curdate() - `to`)'), '<=', 3],
            ['status', 2]
        ])->whereHas('contingents', function (Builder $query) use ($person) {
            $query->whereRaw("((sv_contingents.id_categ1 & ?) > 0 OR (sv_contingents.id_categ2 & ?) > 0)", [intval($person['categ1']), intval($person['categ2'])]);
        });

        $selectRaw = 'sv_surveys.*';
        $surveys = Survey::query()->selectRaw($selectRaw . ($user->isAdmin() ? ', count(sv_user_in_group.id_grp) as is_accessible' : ''));
        if ( $user->isAdmin() ) {
            $surveys = $surveys->leftJoin('user_groups as user_in_group', function($join) use ($userGroups) {
                $join->on('surveys.created_user','=', 'user_in_group.guid_usr')->
                    whereIn('user_in_group.id_grp', $userGroups);
            })->leftJoin('user_groups as user_not_in_group', function($join) use ($userGroups) {
                $join->on('surveys.created_user','=', 'user_not_in_group.guid_usr')->
                    whereNotIn('user_not_in_group.id_grp', $userGroups);
            })->whereIn('user_in_group.id_grp', $userGroups)->orWhere(
                function($query) use ($userGroups, $whereSurveyContingents) {
                    $whereSurveyContingents($query->whereNotIn('user_not_in_group.id_grp', $userGroups));
                }
            )->groupBy('surveys.id');
        } else {
            $surveys = $whereSurveyContingents($surveys);
        }

        $surveys = $surveys->withExists([
            'answers' => function($query) use ($user) {
                return $query->where('guid_user', $user['guid']);
            }
        ])->with('contingents')->orderBy('from', 'desc')->get();

        return $surveys->filter(fn($survey) =>
            $survey->is_accessible ? true : app(SurveyContingentFilter::class)->isAccessedByContingent($survey->contingents)
        )->map(
            fn($survey) => $survey->makeHidden('contingents')
        );
    }

    public function countAll()
    {
        $user = auth()->user();
        if ( !Cache::store('apc')->tags(['surveys_count'])->has($user['guid']) ) {
            $person = $user->person;
            $surveys = Survey::query()->select(['id', 'status'])->where([
                ['from', '<=', now()->toDateString()],
                ['to', '>=', now()->toDateString()],
                ['status', 2]
            ])->withExists([
                'answers' => function($query) use ($user) {
                    return $query->where('guid_user', $user['guid']);
                }
            ])->whereHas('contingents', function (Builder $query) use ($person) {
                $query->whereRaw("((sv_contingents.id_categ1 & ?) > 0 OR (sv_contingents.id_categ2 & ?) > 0)", [intval($person['categ1']), intval($person['categ2'])]);
            })->with('contingents')->get();

            $surveys = $surveys->filter(
                fn($survey) => app(SurveyContingentFilter::class)->isAccessedByContingent($survey->contingents) && !$survey['answers_exist']
            );

            Cache::store('apc')->tags(['surveys_count'])->put($user['guid'], count($surveys), config('cabinet.cache_day_lifetime'));
        }

        return Cache::store('apc')->tags(['surveys_count'])->get($user['guid']);
    }

    public function getById($id, Request $request)
    {
        $query = Survey::query();
        $data = $request->data;
        switch($data) {
            case 'survey':
                $query->with(['contingents', 'questions.variants'])->withExists(['answers'=>
                    fn($query) => $query->where('guid_user', Auth::user()->guid)
                ]);
                break;
            case 'answers':
                $query->with(['contingents', 'questions.variants.answers' =>
                    fn($query) => $query->select(['answers.id_survey', 'answers.id_ques', 'answers.id_var', 'answers.text'])->where('guid_user', '=', Auth::user()->guid)
                ])->withExists(['answers' =>
                    fn($query) => $query->where('guid_user', Auth::user()->guid)
                ]);
                break;
            case 'edit':
                $query->with(['contingents','questions.variants']);
                break;
            case 'result':
                $query->with(['questions.variants' =>
                    fn($query) => $query->withCount('answers'),
                ])->withCount(['answers as respondents_count' =>
                    fn($query) => $query->select(DB::raw('count(distinct guid_user)'))
                ]);
        }
        $survey = $query->findOrFail($id);

        if ($data === 'answers') {
            if ( Auth::user()->role !== 'admin' && !app(SurveyContingentFilter::class)->isAccessedByContingent($survey['contingents']) )
            {
                throw new NoCredentialsException(trans('messages.survey.access_denied'));
            }
            $survey->makeHidden('contingents');
        }

        return $survey;
    }

    public function create(FormRequest $request)
    {
        return Survey::create(
            $request->only($this->fields)['survey'] + [
                'created_user' => Auth::user()->guid,
                'updated_user' => Auth::user()->guid
            ]
        );
    }

    public function update($id, FormRequest $request)
    {
        $survey = Survey::findOrFail($id);
        $survey->update(
            $request->only($this->fields)['survey'] +
            ['updated_user' => Auth::user()->guid]
        );
        return $survey;
    }

    public function copy($id) {
        $survey = Survey::with(['contingents', 'questions.variants'])->findOrFail($id)->replicate();
        $survey->status = 1;
        $survey->title = trans('labels.copy') . ' ' . $survey->title;
        $survey->created_user = Auth::user()->guid;
        $survey->updated_user = Auth::user()->guid;
        $survey->save();

        $survey->contingents()->createMany($survey->contingents->toArray());

        foreach($survey->questions as $question) {
            $copiedQuestion = $survey->questions()->create($question->toArray());
            $copiedQuestion->variants()->createMany($question->variants->toArray());
        }

        $survey->makeHidden('contingents');
        $survey->makeHidden('questions');
        return $survey;
    }
}
