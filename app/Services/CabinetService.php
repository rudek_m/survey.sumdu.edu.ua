<?php


namespace App\Services;


use App\Filters\Cabinet\FieldsForAutocompleteFilter;
use App\Filters\Cabinet\SearchByAttributeFilter;
use App\Repositories\CabinetRepository;
use Illuminate\Pipeline\Pipeline;

class CabinetService
{
    private CabinetRepository $cabinetRepository;
    private array $contingents;


    private array $directionOfTraining = [
        //1, // напрям
        2, //спеціальність
        //3, //спеціалізація
        //4, //профілізація
        //5, // галузь знань
        //6, //підпрофілізація
        //7, //галузь наук
        //8, //індивідуальна траєкторія
        9, //Освітня програма
        10, //Освітньо-професійна програма
        11 //Освітньо-наукова програма
    ];

    public function __construct(CabinetRepository $cabinetRepository)
    {
        $this->cabinetRepository = $cabinetRepository;

        $getFromCabinet = fn($contingentName) => $this->cabinetRepository->{'get'.ucfirst(strtolower($contingentName))}();
        $getFromLocales = fn($contingentName) => trans('labels.contingent.'.strtolower($contingentName));

        $this->contingents = [
            'departments' => [
                'data' => $getFromCabinet,
                'attrs' => [ 'id' => 'ID_DIV', 'title' => 'NAME_DIV']
            ],
            'specialties' => [
                'data' => fn($contingentName) => collect($getFromCabinet($contingentName))->filter(
                        fn($specialty) => in_array($specialty['KOD_INCL'], $this->directionOfTraining)
                    )->toArray(),
                'attrs' => ['id' => 'ID_PROF', 'title' => 'CODE_NAME_PROF', 'description' => 'NAME_INCL' ]
            ],
            'degrees' => [
                'data' => $getFromLocales,
                'attrs' => [ 'id' => 'id', 'title' => 'name' ]
            ],
            'groups' => [
                'data' => $getFromCabinet,
                'attrs' => [ 'id' => 'ID_GROUP', 'title' => 'NAME_GROUP' ]
            ],
            'form' => [
                'data' => $getFromLocales,
                'attrs' => ['id' => 'id', 'title' => 'name']
            ],
            'kod_std' => [
                'data' => $getFromLocales,
                'attrs' => ['id' => 'id', 'title' => 'name']
            ],
            'courses' => [
                'data' => $getFromLocales,
                'attrs' => [ 'id' => 'id', 'title' => 'name' ]
            ]
        ];
    }

    public function autocomplete($contingentName, $search) {
        return app(Pipeline::class)
            ->send($this->contingents[$contingentName]['data']($contingentName))
            ->through(
                new SearchByAttributeFilter($search, $this->contingents[$contingentName]['attrs']['title']),
                app(FieldsForAutocompleteFilter::class, $this->contingents[$contingentName]['attrs'])
            )->thenReturn();
    }
}
