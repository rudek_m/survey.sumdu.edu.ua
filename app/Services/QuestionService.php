<?php


namespace App\Services;


use App\Models\Question;
use Illuminate\Foundation\Http\FormRequest;

class QuestionService
{
    private array $questionFields = [
        'question.text',
        'question.type',
        'question.id_survey',
    ];

    private function createVariants(Question $questionModel, FormRequest $request) {
        $questionModel->variants()->createMany(
            array_map(
                fn($variant) => ['text' => $variant],
                $questionModel['type'] === 'text' ? [null] : $request->only('variants')['variants']
            )
        );
    }

    public function create(FormRequest $request)
    {
        $question = Question::create($request->only($this->questionFields)['question']);
        $this->createVariants($question, $request);
        return $question->load('variants');
    }

    public function update($id, FormRequest $request)
    {
        $question = Question::findOrFail($id);
        $question->update($request->only(array_filter($this->questionFields, fn($field) => $field !== 'question.id_survey'))['question']);
        $this->createVariants($question, $request);
        return $question->load('variants');
    }

}
