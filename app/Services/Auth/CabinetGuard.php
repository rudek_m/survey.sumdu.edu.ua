<?php


namespace App\Services\Auth;


use App\Exceptions\CabinetAuthKeyException;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;

class CabinetGuard implements Guard
{
    private $request;
    private $provider;
    private $user;

    public function __construct(UserProvider $provider, Request $request)
    {
        $this->request = $request;
        $this->provider = $provider;
        $this->user = NULL;
    }

    /**
     * Determine if the current user is authenticated.
     *
     * @return bool
     */
    public function check()
    {
        return !is_null($this->user());
    }

    /**
     * Determine if the current user is a guest.
     *
     * @return bool
     */
    public function guest()
    {
        return !$this->check();
    }

    /**
     * Get the currently authenticated user.
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function user()
    {
        if (!is_null($this->user) ) {
            return $this->user;
        }

        $user = session('user', null);
        if ( isset($user->person['guid']) ) {
            $this->setUser($user);
            return $this->user;
        }
        return null;
    }

    /**
     * Get the ID for the currently authenticated user.
     *
     * @return int|string|null
     */
    public function id()
    {
        if ($user = $this->user()) {
            return $this->user()->getAuthIdentifier();
        }
    }

    /**
     * Validate a user's credentials.
     *
     * @param array $credentials
     * @return bool
     * @throws CabinetAuthKeyException
     */
    public function validate(array $credentials = [])
    {
        $key = $credentials['key'];
        if ( $key ) {
            if ( !($session_key = session('key')) || $session_key != $key ) {
                session()->invalidate();
                $credentials['sid'] = session()->getId();
                $person = $this->provider->retrieveByCredentials($credentials);
                session(['key' => $key]);
                session(['person' => $person]);
                $user = User::with(['user_groups' =>
                    fn($query) => $query->select(['user_groups.id_grp', 'user_groups.guid_usr']) ])->
                    firstOrCreate(['guid' => $person['guid']]);
                $user->user_groups->makeHidden('guid_usr');
                $user->person = $person;
                session(['user' => $user]);
            }
        } else if ( !session('key') ) {
            throw new CabinetAuthKeyException( trans('auth.key_absent') );
        }

        $user = session('user');
        $this->setUser($user);
        return true;
    }

    /**
     * Set the current user.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @return void
     */
    public function setUser(Authenticatable $user)
    {
        $this->user = $user;
    }
}
