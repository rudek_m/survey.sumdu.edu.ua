<?php

namespace App\Services;

use App\Models\Contingent;
use Illuminate\Foundation\Http\FormRequest;

class ContingentService
{

    public function create(FormRequest $request) {
        $contingent = new Contingent();
        return $contingent->create(
            $request->only($contingent->getFillable())
        );

    }

    public function update($id, FormRequest $request) {
        $contingent = Contingent::findOrFail($id);
        $contingent->update(
            $request->only($contingent->getFillable())
        );
        return $contingent;
    }

}
