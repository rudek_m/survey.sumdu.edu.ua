<?php


namespace App\Repositories;

use App\Exceptions\CabinetResponseException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Closure;

class CabinetRepository
{
    private array $cabinetAsuValidationRules = [
        'status'=> 'required|in:OK,ERROR_API',
        'result' => 'required'
    ];

    private function cabinetAsuRequest($uriConfig, Closure $resultHandler = null)
    {
        if ( !Cache::has($uriConfig) ) {
            $cabinetAsuResponse = HTTP::asForm()->get(config($uriConfig), [
                'key' => config('cabinet.asu_key')
            ])->throw()->json();

            $validator = Validator::make($cabinetAsuResponse, $this->cabinetAsuValidationRules);

            if ($validator->fails()) {
                throw new CabinetResponseException($validator->errors()->first());
            }

            if ($cabinetAsuResponse['status'] === 'ERROR_API') {
                throw new CabinetResponseException($cabinetAsuResponse['result']);
            }

            if ($resultHandler) {
                $cabinetAsuResponse['result'] = $resultHandler($cabinetAsuResponse['result']);
            }

            Cache::put($uriConfig, $cabinetAsuResponse['result'], config('cabinet.cache_day_lifetime'));
        }
        return Cache::get($uriConfig);
    }

    /**
     * @param $key
     * @return array
     * @throws \Illuminate\Http\Client\RequestException
     */
    public function gerPersonInfo($key, $sid) {
        return HTTP::asForm()->get(config('cabinet.get_person_info'), [
            'key' => $key,
            'token' => config('cabinet.service_token'),
            'sid' => $sid
        ])->throw()->json();
    }

    public function getSpecialties() {
        $specialtyHandler = fn($array) => collect($array)->map(function($value){
             $value['CODE_NAME_PROF'] = $value['NAME_PROF'] . ' (' . $value['CODE_PROF'] . ')';
             return $value;
        })->keyBy('ID_PROF')->toArray();
        return $this->cabinetAsuRequest('cabinet.asu_get_specialties', $specialtyHandler);
    }

    public function getGroups() {
        return $this->cabinetAsuRequest('cabinet.asu_get_groups');
    }

    public function getDepartments() {
        return $this->cabinetAsuRequest('cabinet.asu_get_departments');
    }
    /**
     * @return \Illuminate\Http\Client\Response
     * @throws \Illuminate\Http\Client\RequestException
     */
    public function getPersonPhoto($key) {
        return HTTP::get(config('cabinet.get_person_photo'),[
            'key' => $key
        ])->throw();
    }

    /**
     * @param $key
     * @return array
     * @throws \Illuminate\Http\Client\RequestException
     */
    public function logout($key) {
        return HTTP::asForm()->get(config('cabinet.logout_api'), [
            'key' => $key
        ])->throw()->json();

    }
}
