<?php

namespace App\Exports;

use App\Models\Survey;
use Maatwebsite\Excel\Concerns\FromArray;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\StringValueBinder;

class SurveyResultExport extends StringValueBinder implements FromArray, WithCustomValueBinder
{
    private $id;
    private array $contingentAttributes = [
        'name_kod_std',
        'name_div', 'code_div',
        'id_specialty', 'name_specialty', 'code_specialty',
        'id_edu_prog', 'name_edu_prog', 'code_edu_prog',
        'id_group', 'name_group', 'name_degree', 'name_form', 'id_course'
    ];

    public function __construct(int $id) {
        $this->id = $id;
    }

    public function array(): array
    {

        $result = Survey::query()->
            with([
                'questions',
                'variants' => fn($query) =>
                    $query->select(DB::raw("GROUP_CONCAT(IFNULL(sv_variants.text, ''), IFNULL(sv_answers.text, '') ORDER BY sv_variants.id SEPARATOR ';' ) as answers"), 'answers.id_ques', 'answers.guid_user', 'user_survey.*' )->
                    join('answers', 'variants.id', '=', 'answers.id_var')->
                    join('user_survey', [ ['answers.guid_user', '=', 'user_survey.guid_user'], ['answers.id_survey', '=', 'user_survey.id_survey'] ])->
                    groupBy(['answers.guid_user', 'answers.id_ques', 'user_survey.id'])->
                    orderBy('answers.guid_user')
        ])->findOrFail($this->id);//->toArray();

        $variants = [];
        foreach($result['variants']->groupBy('guid_user')->toArray() as $guid_user => $user) {
            $variants[$guid_user] = array_column($user, 'answers');
            $variants[$guid_user] = array_merge($variants[$guid_user], collect($user[0])->only($this->contingentAttributes)->flatten()->toArray());
        }

        return [array_column($result['questions']->toArray(), 'text') + trans('labels.contingent.attributes'),
                $variants];
    }
}
